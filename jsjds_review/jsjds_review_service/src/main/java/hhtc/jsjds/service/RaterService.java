package hhtc.jsjds.service;

import hhtc.jsjds.entity.TaskDetail;

import java.util.Map;

public interface RaterService {
    boolean insertNewRecord(String uid, String wid, String desc, Integer score);

    Map<Integer,String> getOldScore(String uid, String wid);

    boolean updateScore(String wid, String uid, String desc, Integer score);

    TaskDetail getTaskDetail(String uid);

    Boolean insertNewFinalRecord(String uid, String works_id, String recommend, String score);

    boolean updateFinalScore(String uid, String works_id, String recommend, String score);
}
