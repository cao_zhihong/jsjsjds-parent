package hhtc.jsjds.service.impl;

import hhtc.jsjds.entity.TaskDetail;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.exception.XscoderException;
import hhtc.jsjds.mapper.RaterMapper;
import hhtc.jsjds.service.RaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class RaterServiceImpl implements RaterService {

    @Autowired
    RaterMapper raterMapper;

    @Override
    public boolean insertNewRecord(String uid, String wid, String desc, Integer score) {
        int i = raterMapper.insertNewRecord(uid,wid,desc,score+"");
        return true;
    }

    @Override
    public Map<Integer, String> getOldScore(String uid, String wid) {
        return raterMapper.getOldScore(uid,wid);
    }

    @Override
    public boolean updateScore(String wid, String uid, String desc, Integer score) {
        int i = raterMapper.updateScore(wid,uid,desc,score+"");
        return true;
    }

    @Override
    public TaskDetail getTaskDetail(String uid) {
        TaskDetail detail = new TaskDetail();
        detail.setTotal(raterMapper.getTotalCount(uid));
        detail.setCompleted(raterMapper.getCompletedCount(uid));
        detail.setUncompleted(detail.getTotal()-detail.getCompleted());
        return detail;
    }

    @Override
    public Boolean insertNewFinalRecord(String uid, String works_id, String recommend, String score) {
        try {
            raterMapper.insertNewFinalRecord(uid,works_id,recommend+"",score+"");
        }catch (Exception e){
            throw new XscoderException(StatusCodeEnum.RATE_HAS_FINISHED);
        }
        return true;
    }

    @Override
    public boolean updateFinalScore(String uid, String works_id, String recommend, String score) {
        int i = raterMapper.updateFinalScore(uid,works_id,recommend,score);
        return true;
    }
}
