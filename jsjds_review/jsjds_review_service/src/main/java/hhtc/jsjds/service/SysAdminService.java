package hhtc.jsjds.service;

import hhtc.jsjds.entity.User;

public interface SysAdminService {
    User addNewRate(User user, Integer[] categories);

    User addFinalSeqAccount(User user, Integer[] categoriesSelect);
}
