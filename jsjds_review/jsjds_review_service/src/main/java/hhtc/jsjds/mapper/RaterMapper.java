package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface RaterMapper extends Mapper<User> {


    int addNewRater(User user);
    int add2Role(User user);
    int add2RaterToCategory(String uid,String cid);

    int insertNewRecord(String uid, String wid, String desc, String score);

    Map<Integer, String> getOldScore(String uid, String wid);

    int updateScore(String wid, String uid, String desc, String score);

    Integer getTotalCount(String uid);

    Integer getCompletedCount(String uid);

    int insertNewFinalRecord(String uid, String works_id, String recommend, String score);

    int updateFinalScore(String uid, String works_id, String recommend, String score);

    void addFinalSeqAccount(User user);

    void addFinalAccountCategory(@Param("id") String id, @Param("areaId") Integer areaId);

    List<Integer> getAllAreaId(@Param("idList") List<Integer> idList);
}
