package hhtc.jsjds.controller;


import hhtc.jsjds.client.CompetitionFeignClient;
import hhtc.jsjds.entity.Works;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.exception.XscoderException;
import hhtc.jsjds.service.RaterService;
import hhtc.jsjds.utils.JwtCommonUtils;
import hhtc.jsjds.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/final")
public class FinalController {

    @Autowired
    CompetitionFeignClient competitionFeignClient;

    @Autowired
    RaterService raterService;


    @GetMapping("/works")
    public Result getWorksByTeamId( @RequestParam(required = true) String team_id){
        Works works = competitionFeignClient.getWorksByTeamId(team_id);
        if(works==null||works.getId()==null){
            throw new XscoderException(StatusCodeEnum.TEAM_NOT_FOUND);
        }
        return new Result(true, StatusCodeEnum.OK,works);
    }

    @PostMapping("/rate")
    public Result rateWorks(HttpServletRequest request,@RequestParam(required = true) String works_id,@RequestParam(required = true) Integer recommend,@RequestParam(required = true) Integer score){
        String uid = JwtCommonUtils.getIDFromToken(request);
        Boolean flag = raterService.insertNewFinalRecord(uid,works_id,recommend+"",score+"");
        return new Result(true,StatusCodeEnum.OK);
    }

    @GetMapping("/completed")
    public Result getAllCompletedWorks(HttpServletRequest request){
        List<Works> list = competitionFeignClient.getFinalAllCompletedWorks();
        return new Result(true, StatusCodeEnum.OK,list);
    }

    @PutMapping("/rate")
    public Result updateScore(HttpServletRequest request,@RequestParam(required = true) String works_id,@RequestParam(required = true) Integer recommend,@RequestParam(required = true) Integer score){
        String uid = JwtCommonUtils.getIDFromToken(request);
        boolean flag = raterService.updateFinalScore(uid,works_id,recommend+"",score+"");
        return new Result(true,StatusCodeEnum.OK);
    }
}
