package hhtc.jsjds.controller;


import hhtc.jsjds.client.CompetitionFeignClient;
import hhtc.jsjds.entity.TaskDetail;
import hhtc.jsjds.entity.Works;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.RaterService;
import hhtc.jsjds.utils.JwtCommonUtils;
import hhtc.jsjds.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/review")
public class ReviewController {


    @Autowired
    CompetitionFeignClient competitionFeignClient;

    @Autowired
    RaterService raterService;

    @GetMapping("/tasks10")
    public Result getAllCategories(){
        List<Works> list = competitionFeignClient.getTasks10();
        return new Result(true, StatusCodeEnum.OK,list);
    }

    @GetMapping("/completed")
    public Result getAllCompletedWorks(HttpServletRequest request){
        List<Works> list = competitionFeignClient.getAllCompletedWorks();
        return new Result(true, StatusCodeEnum.OK,list);
    }

    @PostMapping("/rate")
    public Result rateWorks(HttpServletRequest request, @RequestParam(required = true) String wid,@RequestParam(required = true) String desc,@RequestParam(required = true) Integer score){
        String uid = JwtCommonUtils.getIDFromToken(request);
        raterService.insertNewRecord(uid,wid,desc,score);
        boolean flag;
        if(score<60){
            flag = competitionFeignClient.setTeamEliminatedByWid(wid);
        }else{
            flag = competitionFeignClient.setTeamPassedByWid(wid);
        }
        return new Result(flag, StatusCodeEnum.OK);
    }

    @GetMapping("/oldscore")
    public Result getOldScore(HttpServletRequest request, @RequestParam(required = true) String wid){
        String uid = JwtCommonUtils.getIDFromToken(request);
        return new Result(true,StatusCodeEnum.OK,raterService.getOldScore(uid,wid));
    }

    @PutMapping("/rate")
    public Result updateScore(HttpServletRequest request,@RequestParam(required = true) String wid,@RequestParam(required = true) String desc,@RequestParam(required = true) Integer score){
        String uid = JwtCommonUtils.getIDFromToken(request);
        raterService.updateScore(wid,uid,desc,score);
        return new Result(true,StatusCodeEnum.OK);
    }

    @GetMapping("/taskdetail")
    public  Result getTaskDetail(HttpServletRequest request){
        String uid = JwtCommonUtils.getIDFromToken(request);
        TaskDetail deail = raterService.getTaskDetail(uid);
        return new Result(true,StatusCodeEnum.OK,deail);
    }



}
