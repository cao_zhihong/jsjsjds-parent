package hhtc.jsjds.controller;


import com.github.pagehelper.PageInfo;
import hhtc.jsjds.client.CompetitionFeignClient;
import hhtc.jsjds.entity.User;
import hhtc.jsjds.entity.Works;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.SysAdminService;
import hhtc.jsjds.utils.CodeUtils;
import hhtc.jsjds.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/sysadmin")
public class SysAdminController {

    @Autowired
    CompetitionFeignClient competitionFeignClient;

    @Autowired
    SysAdminService sysAdminService;

    @GetMapping("/category")
    public Result getAllCategories() {
        return competitionFeignClient.getAllCategories();
    }

    @PostMapping("/rater")
    public Result addNewRate(String pass, String name, Integer[] categoriesSelect, @RequestParam Integer type) {
        User user = new User();
        user.setUsername("HHTC_" + CodeUtils.generateCode(6));
        user.setRealname(name);
        user.setPassword(pass);
        User u;
        if (type == 1) {
            u = sysAdminService.addNewRate(user, categoriesSelect);
        } else {
            u = sysAdminService.addFinalSeqAccount(user, categoriesSelect);
        }
        u.setPassword("");
        return new Result(true, StatusCodeEnum.OK, u);
    }

    @GetMapping("/works")
    public Result getAllWorks(@RequestParam String page, @RequestParam String pageSize) {
        PageInfo pageInfo = competitionFeignClient.sysAdminGetAllWorks(page, pageSize);
        return new Result(true, StatusCodeEnum.OK, pageInfo);
    }

    @PostMapping("/workspassed")
    public Result setWorksPassed(@RequestParam String wid) {
        competitionFeignClient.setWorksPassed(wid);
        return new Result(true, StatusCodeEnum.OK);
    }

    @GetMapping("/completed")
    public Result getSYSAllCompletedWorks(HttpServletRequest request, String team_id) {
        List<Works> list = competitionFeignClient.getSYSAllCompletedWorks(team_id);
        return new Result(true, StatusCodeEnum.OK, list);
    }


}
