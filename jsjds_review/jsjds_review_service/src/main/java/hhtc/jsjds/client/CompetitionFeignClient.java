package hhtc.jsjds.client;

import com.github.pagehelper.PageInfo;
import hhtc.jsjds.client.Interceptor.FeignHeadersInterceptor;
import hhtc.jsjds.client.fallback.CompetitionFeignClientFallback;
import hhtc.jsjds.entity.Works;
import hhtc.jsjds.vo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "user-service", fallback = CompetitionFeignClientFallback.class,configuration = FeignHeadersInterceptor.class)
public interface CompetitionFeignClient {

    @GetMapping("/competition/category")
    Result getAllCategories();


    @GetMapping("/competition/tasks10")
    List<Works> getTasks10();

    @GetMapping("/competition/completed")
    List<Works> getAllCompletedWorks();

    @PostMapping("/competition/eliminate")
    boolean setTeamEliminatedByWid(@RequestParam  String wid);

    @GetMapping("/competition/sysadminworks")
    PageInfo sysAdminGetAllWorks(@RequestParam String page, @RequestParam String pageSize);

    @PostMapping("/competition/passed")
    boolean setTeamPassedByWid(@RequestParam String wid);

    @PostMapping("/competition/workspassed")
    boolean setWorksPassed(@RequestParam String wid);

    @GetMapping("/competition/finalworks")
    Works getWorksByTeamId(@RequestParam String team_id);

    @GetMapping("/competition/finalcompleted")
    List<Works> getFinalAllCompletedWorks();

    @GetMapping("/competition/sysfinalcompleted")
    List<Works> getSYSAllCompletedWorks(@RequestParam String team_id);
}
