package hhtc.jsjds.client.fallback;

import com.github.pagehelper.PageInfo;
import hhtc.jsjds.client.CompetitionFeignClient;
import hhtc.jsjds.entity.Works;
import hhtc.jsjds.vo.Result;

import java.util.List;

public class CompetitionFeignClientFallback implements CompetitionFeignClient {

    @Override
    public Result getAllCategories() {
        return null;
    }

    @Override
    public List<Works> getTasks10() {
        return null;
    }

    @Override
    public List<Works> getAllCompletedWorks() {
        return null;
    }

    @Override
    public boolean setTeamEliminatedByWid(String wid) {
        return false;
    }

    @Override
    public PageInfo sysAdminGetAllWorks(String page, String pageSize) {
        return null;
    }

    @Override
    public boolean setTeamPassedByWid(String wid) {
        return false;
    }

    @Override
    public boolean setWorksPassed(String wid) {
        return false;
    }

    @Override
    public Works getWorksByTeamId(String team_id) {
        return null;
    }

    @Override
    public List<Works> getFinalAllCompletedWorks() {
        return null;
    }

    @Override
    public List<Works> getSYSAllCompletedWorks(String team_id) {
        return null;
    }
}
