package hhtc.jsjds.entity;


import lombok.Data;

@Data
public class TaskDetail {
    private Integer total;
    private Integer completed;
    private Integer uncompleted;
}
