package hhtc.jsjds.securityhandler;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * 
 * @author X_S
 *
 */
public class GoLogoutSuccessHandler implements LogoutSuccessHandler {
    @Override
    @CrossOrigin
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
//        RestResult restResult=new RestResult(true,"200"200,"注销成功",null);
        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD");
        httpServletResponse.setHeader("Content-Type", "application/json;charset=utf-8");
        Map restResult = new HashMap();
        restResult.put("msg","onLogoutSuccess，无法访问该资源");
        httpServletResponse.getWriter().write(JSON.toJSONString(restResult));
        httpServletResponse.getWriter().flush();
 
    }
}
