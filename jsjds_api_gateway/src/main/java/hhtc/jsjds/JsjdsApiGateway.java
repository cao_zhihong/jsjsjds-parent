package hhtc.jsjds;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


@SpringCloudApplication
@EnableZuulProxy
public class JsjdsApiGateway {
    public static void main(String[] args) {
        SpringApplication.run(JsjdsApiGateway.class, args);
    }
}

