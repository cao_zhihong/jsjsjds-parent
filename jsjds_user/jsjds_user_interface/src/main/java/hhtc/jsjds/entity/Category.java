package hhtc.jsjds.entity;


import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Data
@Table(name = "hhtc_category")
public class Category {
    @Id
    @KeySql(useGeneratedKeys = true)
    private String id;
    private String name;
    private String parent_id;
    private Integer count_limit;

    @Transient
    private List<Category> categories;
}
