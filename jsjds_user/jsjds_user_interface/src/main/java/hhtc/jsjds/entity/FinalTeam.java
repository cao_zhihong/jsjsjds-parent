package hhtc.jsjds.entity;


import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "hhtc_final_seq")
public class FinalTeam {
    @Id
    @KeySql(useGeneratedKeys = true)
    private String team_id;
    private String sequence;
    private String area;
}
