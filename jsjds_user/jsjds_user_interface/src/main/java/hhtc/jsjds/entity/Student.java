package hhtc.jsjds.entity;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Table(name = "student")
public class Student {
    @Id
    @KeySql(useGeneratedKeys = true)
    private String stu_id;
    private String stu_name;
    private String stu_number;
    private String academy_id;
    private String major_id;
    private String id_number;
    private String stu_idCard;
    private String stu_mail;
    private String stu_phone;
    private String stu_qq;
    private String uid;
    private String createtime;
    @Transient
    private Academy academy;
    @Transient
    private Academy major;
}
