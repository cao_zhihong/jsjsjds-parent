package hhtc.jsjds.entity;


import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "student")
public class User {
    @Id
    @KeySql(useGeneratedKeys = true)
    private String id;
    private String username;
    private String realname;
    private String password;
    private String avatar;
}
