package hhtc.jsjds.entity;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Data
@Table(name = "hhtc_academy")
public class Academy {

    @Id
    @KeySql(useGeneratedKeys = true)
    private String id;
    private String academy_name;
    private String parent_id;
    @Transient
    private List<Academy> academies;

}
