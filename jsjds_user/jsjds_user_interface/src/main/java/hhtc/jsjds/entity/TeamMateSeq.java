package hhtc.jsjds.entity;


import lombok.Data;

@Data
public class TeamMateSeq {
    private String uid;
    private Integer index;
}
