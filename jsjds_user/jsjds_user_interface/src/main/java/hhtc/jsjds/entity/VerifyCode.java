package hhtc.jsjds.entity;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;


@Data
@Table(name = "hhtc_verifycode")
public class VerifyCode {
    @Id
    @KeySql(useGeneratedKeys = true)
    String file_name;
    String code;
}
