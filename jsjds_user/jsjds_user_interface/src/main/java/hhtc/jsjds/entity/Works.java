package hhtc.jsjds.entity;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Table(name = "hhtc_works")
public class Works {
    @Id
    @KeySql(useGeneratedKeys = true)
    private String id;
    private String team_id;
    private String works_name;
    private String works_url;
    private String works_key;
    private String works_img_url;
    private String category_id;
    private String competition_area;
    private String works_introduction;
    private String operating_instructions;
    private String installation_instructions;
    private String design_thought;
    private String other_description;
    private String instructor_self;
    private String design_point;
    private String works_create_time;
    private String works_update_time;
    private String teacher_name;

    @Transient
    private Academy academy;
    @Transient
    private Category category;
    @Transient
    private String uid;

    @Transient
    private Integer score;
    @Transient
    private String desc;
    @Transient
    private String recommend;
    @Transient
    private User rater;

    private String state;
}
