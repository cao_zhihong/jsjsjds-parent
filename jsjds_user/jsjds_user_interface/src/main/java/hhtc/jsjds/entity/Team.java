package hhtc.jsjds.entity;


import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Table(name = "hhtc_team")
public class Team {
    @Id
    @KeySql(useGeneratedKeys = true)
    private String id;
    private String uid;
    private String Teacher;
    @Column(name="categoryA_id")
    private String categoryA_id;
    @Column(name="categoryB_id")
    private String categoryB_id;
    private String invitation_code;
    private String createtime;


    @Transient
    private Student owner;


    @Transient
    private Category categoryA;
    @Transient
    private Category categoryB;
}
