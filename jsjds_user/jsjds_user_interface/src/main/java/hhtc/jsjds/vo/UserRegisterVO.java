package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/19 15:53
 */
@Data
public class UserRegisterVO {
    private String username;
    private String password;
    private String phone;
}
