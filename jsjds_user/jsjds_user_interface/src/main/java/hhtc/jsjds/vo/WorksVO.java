package hhtc.jsjds.vo;

import hhtc.jsjds.entity.Academy;
import hhtc.jsjds.entity.Category;
import hhtc.jsjds.entity.User;
import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/21 9:57
 */
@Data
public class WorksVO {
    private String id;
    private String team_id;
    private String works_name;
    private String works_url;
    private String works_key;
    private String works_img_url;
    private String category_id;
    private String competition_area;
    private String works_introduction;
    private String operating_instructions;
    private String installation_instructions;
    private String design_thought;
    private String other_description;
    private String instructor_self;
    private String design_point;
    private String works_create_time;
    private String works_update_time;
    private String teacher_name;

    private Academy academy;
    private Category category;
    private String uid;
    private String imagestring;

    private Integer score;
    private String desc;
    private String recommend;
    private User rater;

    private String state;
}
