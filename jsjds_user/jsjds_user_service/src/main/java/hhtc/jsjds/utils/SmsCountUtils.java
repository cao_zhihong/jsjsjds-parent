package hhtc.jsjds.utils;

import hhtc.jsjds.constant.RedisKey;
import hhtc.jsjds.redis.RedisProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/21 13:47
 */
@Component
@Slf4j
public class SmsCountUtils {

    @Autowired
    private RedisProvider redisProvider;

    @Scheduled(cron = "0 0 4 * * ?")
    public void removeSmsCount() {
        try {
            long start = System.currentTimeMillis();
            log.info("==============清除用户短信发送次数定时任务开始==================");
            String redisKey = RedisKey.VERIFICATION_COUNT + "*";
            Set<String> keys = redisProvider.keys(redisKey);
            for (String key : keys) {
                redisProvider.delete(key);
            }
            long end = System.currentTimeMillis();
            log.info("==============清除用户短信发送次数定时任务结束，一共耗时{}ms", end - start);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("==============清除用户短信发送次数定时任务出现异常{}====", e);
        }

    }


}
