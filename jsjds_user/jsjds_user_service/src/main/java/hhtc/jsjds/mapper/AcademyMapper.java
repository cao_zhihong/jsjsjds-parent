package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.Academy;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface AcademyMapper extends Mapper<Academy> {

}
