package hhtc.jsjds.mapper;


import hhtc.jsjds.entity.Works;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface WorksMapper extends Mapper<Works> {

    List<Works> getTasks10(String uid);

    List<Works> getAllCompletedWorks(String uid);

    List<Works> sysAdminGetAllWorks();

    List<Works> getWorksByTeamId(String uid, String team_id);

    List<Works> getFinalAllCompletedWorks(String uid);

    List<Works> getSYSAllCompletedWorks(Map map);

    void updateReviewScore(String wid, Integer score);
}
