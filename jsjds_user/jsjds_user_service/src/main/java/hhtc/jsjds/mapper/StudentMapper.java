package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.Student;
import hhtc.jsjds.entity.User;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;


@Repository
public interface StudentMapper extends Mapper<Student> {
    int add2User(User user);

    int add2Student(Student student);

    int add2Role(User user);
}
