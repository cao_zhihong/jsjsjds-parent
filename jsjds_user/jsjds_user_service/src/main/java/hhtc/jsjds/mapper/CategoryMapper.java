package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.Category;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;


@Repository
public interface CategoryMapper extends Mapper<Category> {
}
