package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.VerifyCode;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface VerifyCodeMapper extends Mapper<VerifyCode> {
}
