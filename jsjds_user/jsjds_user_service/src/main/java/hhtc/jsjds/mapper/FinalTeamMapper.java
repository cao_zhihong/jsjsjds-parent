package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.FinalTeam;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface FinalTeamMapper extends Mapper<FinalTeam> {

}
