package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.Student;
import hhtc.jsjds.entity.Team;
import hhtc.jsjds.entity.TeamMateSeq;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;


@Repository
public interface TeamMapper extends Mapper<Team> {
    int insert2link(String uid,String tid,String index);

    int testAuth(String academy_id, String categoryA_id);

    List<Integer> getCategories(String uid);

    List<Team> GetUserOtherTeams(String uid);

    int CountTeamByUid(String uid);

    int CountStuByTid(String id);

    int delete2link(String tid);

    List<Student> getTeamMate(String tid);

    List<Integer> getTeamUids(String tid);

    List<Integer> getAllowList(String cid);

    int userExitTeam(String uid, String tid);

    List<TeamMateSeq> getTeamIndex(String tid);

    int saveTeamIndex(String uid, String index, String tid);

    int setTeamEliminatedByWid(String wid);

    String getTidByWid(String wid);

    void deleteTeamWorks(String tid);
}
