package hhtc.jsjds.service.impl;

import hhtc.jsjds.entity.Team;
import hhtc.jsjds.entity.Works;
import hhtc.jsjds.mapper.TeamMapper;
import hhtc.jsjds.mapper.WorksMapper;
import hhtc.jsjds.service.WorksService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WorksServiceImpl implements WorksService {

    @Autowired
    WorksMapper worksMapper;

    @Autowired
    TeamMapper teamMapper;

    @Override
    public List<Works> getAllWorks(){
        List<Works> list = worksMapper.selectAll();
        return list;
    }


    @Override
    public Works getWorksByTeam_id(String team_id) {
        Example example = new Example(Works.class);
        example.createCriteria().andEqualTo("team_id",team_id);
        Works works = worksMapper.selectOneByExample(example);
        return works;
    }
    @Override
    public int setWorks(Works works){
        return  worksMapper.insert(works);
    }

    @Override
    public Boolean updateWorks(Works works){
        Example example = new Example(Works.class);
        example.createCriteria().andEqualTo("team_id",works.getTeam_id());
        Works oldWorks = worksMapper.selectOneByExample(example);
        oldWorks.setWorks_img_url(works.getWorks_img_url());
        if(!StringUtils.isBlank(works.getWorks_name())){
            oldWorks.setWorks_name(works.getWorks_name());
        }
        if(!StringUtils.isBlank(works.getWorks_url())) {
            oldWorks.setWorks_url(works.getWorks_url());
        }
        if(!StringUtils.isBlank(works.getWorks_key())){
            oldWorks.setWorks_key(works.getWorks_key());
        }
        if(!StringUtils.isBlank(works.getCompetition_area())){
            oldWorks.setCompetition_area(works.getCompetition_area());
        }
        if(!StringUtils.isBlank(works.getDesign_point())){
            oldWorks.setDesign_point(works.getDesign_point());
        }
        if(!StringUtils.isBlank(works.getDesign_thought())){
            oldWorks.setDesign_thought(works.getDesign_thought());
        }
        if(!StringUtils.isBlank(works.getInstallation_instructions())){
            oldWorks.setInstallation_instructions(works.getInstallation_instructions());
        }
        if(!StringUtils.isBlank(works.getInstructor_self())){
            oldWorks.setInstructor_self(works.getInstructor_self());
        }
        if(!StringUtils.isBlank(works.getOperating_instructions())){
            oldWorks.setOperating_instructions(works.getOperating_instructions());
        }
        if(!StringUtils.isBlank(works.getOther_description())){
            oldWorks.setOther_description(works.getOther_description());
        }
        if(!StringUtils.isBlank(works.getWorks_introduction())){
            oldWorks.setWorks_introduction(works.getWorks_introduction());
        }
        if(works.getWorks_update_time() != null){
            oldWorks.setWorks_update_time(works.getWorks_update_time());
        }
        oldWorks.setTeacher_name(works.getTeacher_name());
        return  worksMapper.updateByPrimaryKey(oldWorks)==1;
    }

    @Override
    public Team getTeamIndex(String team_id , String uid){
        Example example = new Example(Team.class);
        example.createCriteria().andEqualTo("id",team_id).andEqualTo("uid",uid);
        return teamMapper.selectOneByExample(example);
    }

    @Override
    public List<Works> getTasks10(String uid) {
        return worksMapper.getTasks10(uid);
    }

    @Override
    public List<Works> getAllCompletedWorks(String uid) {
        return worksMapper.getAllCompletedWorks(uid);
    }

    @Override
    public List<Works> getFinalAllCompletedWorks(String uid) {
        return worksMapper.getFinalAllCompletedWorks(uid);
    }

    @Override
    public List<Works> getSYSAllCompletedWorks(String team_id) {
        Map<String,String> map = new HashMap<String,String>();
        map.put("team_id",team_id);
        return worksMapper.getSYSAllCompletedWorks(map);
    }
}
