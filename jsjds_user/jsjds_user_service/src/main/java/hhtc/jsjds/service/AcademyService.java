package hhtc.jsjds.service;

import hhtc.jsjds.entity.Academy;

import java.util.List;

public interface AcademyService {
    List<Academy> queryAcademyByName(String s);

}
