package hhtc.jsjds.service.impl;

import hhtc.jsjds.entity.VerifyCode;
import hhtc.jsjds.mapper.VerifyCodeMapper;
import hhtc.jsjds.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    VerifyCodeMapper verifyCodeMapper;

    @Override
    public VerifyCode getVerifyCode(VerifyCode verifyCode) {
        return verifyCodeMapper.selectOne(verifyCode);
    }
}
