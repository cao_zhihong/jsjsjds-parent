package hhtc.jsjds.service.impl;


import hhtc.jsjds.entity.Academy;
import hhtc.jsjds.mapper.AcademyMapper;
import hhtc.jsjds.service.AcademyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class AcademyServiceImpl implements AcademyService {

    @Autowired
    AcademyMapper academyMapper;

    @Override
    public List<Academy> queryAcademyByName(String s) {
        Example example = new Example(Academy.class);
        example.createCriteria().andEqualTo("parent_id","0")
                .andLike("academy_name",s);
        List<Academy> academies = academyMapper.selectByExample(example);
        for(Academy academy:academies){
            Academy temp = new Academy();
            temp.setParent_id(academy.getId());
            academy.setAcademies(academyMapper.select(temp));
        }
        return academies;
    }
}
