package hhtc.jsjds.service.impl;

import hhtc.jsjds.entity.Academy;
import hhtc.jsjds.entity.Student;
import hhtc.jsjds.entity.User;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.exception.XscoderException;
import hhtc.jsjds.mapper.AcademyMapper;
import hhtc.jsjds.mapper.StudentMapper;
import hhtc.jsjds.service.StudentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class StudentServiceImpl implements StudentService {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    StudentMapper studentMapper;

    @Autowired
    AcademyMapper academyMapper;


    @Override
    @Transactional
    public Boolean InsertNewStudent(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder().encode(password));
        Student student = new Student();
        try {
            studentMapper.add2User(user);
            student.setStu_number(user.getUsername());
            student.setUid(user.getId());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            student.setCreatetime(sdf.format(new Date()));
            studentMapper.add2Student(student);
            studentMapper.add2Role(user);
        }catch (Exception e){
            throw new XscoderException(StatusCodeEnum.STUDENT_HAS_REGISTED);
        }
        return true;
    }

    @Override
    public Boolean updateStudent(Student student) {
        Example example = new Example(Student.class);
        example.createCriteria().andEqualTo("uid",student.getUid());
        Student oldStudent = studentMapper.selectOneByExample(example);
        if(StringUtils.isBlank(oldStudent.getAcademy_id())){
            oldStudent.setAcademy_id(student.getAcademy_id());
        }
//        if(StringUtils.isBlank(oldStudent.getId_number())){
        oldStudent.setId_number(student.getId_number());
//        }
        if(StringUtils.isBlank(oldStudent.getMajor_id())){
            oldStudent.setMajor_id(student.getMajor_id());
        }
        if(StringUtils.isBlank(oldStudent.getStu_idCard())){
            oldStudent.setStu_idCard(student.getStu_idCard());
        }
//        if(StringUtils.isBlank(oldStudent.getStu_mail())){
        oldStudent.setStu_mail(student.getStu_mail());
//        }
//        if(StringUtils.isBlank(oldStudent.getStu_phone())){
        oldStudent.setStu_phone(student.getStu_phone());
//        }
//        if(StringUtils.isBlank(oldStudent.getStu_name())){
        oldStudent.setStu_name(student.getStu_name());
//        }
//        if(StringUtils.isBlank(oldStudent.getStu_qq())){
        oldStudent.setStu_qq(student.getStu_qq());
//        }
        return    studentMapper.updateByPrimaryKey(oldStudent)==1;
    }

    @Override
    public Student getStudentByUid(String uid) {
        Example example = new Example(Student.class);
        example.createCriteria().andEqualTo("uid",uid);
        Student student = studentMapper.selectOneByExample(example);
        if(!StringUtils.isBlank(student.getAcademy_id())){
            Academy academy = academyMapper.selectByPrimaryKey(student.getAcademy_id());
            Example example1 = new Example(Academy.class);
            example1.createCriteria().andEqualTo("parent_id",academy.getId());
            academy.setAcademies(academyMapper.selectByExample(example1));
            student.setAcademy(academy);
        }
        if(!StringUtils.isBlank(student.getMajor_id())){
            student.setMajor(academyMapper.selectByPrimaryKey(student.getMajor_id()));
        }
        return student;
    }
}
