package hhtc.jsjds.service;

import hhtc.jsjds.entity.Student;

public interface StudentService {
    Boolean InsertNewStudent(String username, String password);

    Boolean updateStudent(Student student);

    Student getStudentByUid(String uid);
}
