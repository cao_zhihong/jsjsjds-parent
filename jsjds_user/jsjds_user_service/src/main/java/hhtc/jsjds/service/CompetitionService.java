package hhtc.jsjds.service;

import com.github.pagehelper.PageInfo;
import hhtc.jsjds.entity.Category;
import hhtc.jsjds.entity.Student;
import hhtc.jsjds.entity.Team;
import hhtc.jsjds.entity.Works;

import java.util.List;

public interface CompetitionService {
    List<Category> getAllCategories();


    boolean insertNewTeam(Team team);


    List<Team> GetUserOwnTeams(String uid);

    List<Team> GetUserOtherTeams(String uid);

    boolean StudentAddTeam(String uid, String invitation_code);

    boolean deleteTeam(String uid, String tid);

    List<Student> getTeamMate(String uid, String tid);

    boolean userExitTeam(String uid, String tid);

    boolean changeTeamMateIndex(String uid, String tid);

    boolean setTeamEliminatedByWid(String wid);


    PageInfo<Works> sysAdminGetAllWorks(Integer page, Integer pageSize);

    boolean setTeamPassedByWid(String wid);

    void randomSort();


    Works getWorksByTeamId(String uid, Integer team_id);

}
