package hhtc.jsjds.service;


import hhtc.jsjds.entity.VerifyCode;

/**
 * @author X_S
 * 放一些通用的方法
 * 1.获取验证码
 */
public interface CommonService {
    VerifyCode getVerifyCode(VerifyCode verifyCode);
}
