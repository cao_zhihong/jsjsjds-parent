package hhtc.jsjds.service;

import hhtc.jsjds.entity.Team;
import hhtc.jsjds.entity.Works;

import java.util.List;

public interface WorksService {

    List<Works> getAllWorks();

    Works getWorksByTeam_id(String Team_id);

    int setWorks(Works works);

    Boolean updateWorks(Works works);

    Team getTeamIndex(String team_id, String uid);

    List<Works> getTasks10(String uid);

    List<Works> getAllCompletedWorks(String uid);

    List<Works> getFinalAllCompletedWorks(String uid);

    List<Works> getSYSAllCompletedWorks(String team_id);

}
