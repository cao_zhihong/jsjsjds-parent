package hhtc.jsjds.controller;

import hhtc.jsjds.constant.RedisKey;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.redis.RedisProvider;
import hhtc.jsjds.vo.Result;
import hhtc.jsjds.vo.TimeConfigUpdateVO;
import hhtc.jsjds.vo.TimeConfigVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/21 11:00
 */
@RestController
@RequestMapping("/time")
public class TimeController {

    @Autowired
    private RedisProvider redisProvider;

    @PostMapping("/set")
    public Result setTime(String enrollEndTime, String uploadStartTime, String uploadEndTime, String replyStartTime) {
        if (StringUtils.isNotBlank(enrollEndTime)) {
            redisProvider.set(RedisKey.ENROLL_END_TIME, enrollEndTime, -1);
        }
        if (StringUtils.isNotBlank(uploadStartTime)) {
            redisProvider.set(RedisKey.UPLOAD_START_TIME, uploadStartTime, -1);
        }
        if (StringUtils.isNotBlank(uploadEndTime)) {
            redisProvider.set(RedisKey.UPLOAD_END_TIME, uploadEndTime, -1);
        }
        if (StringUtils.isNotBlank(replyStartTime)) {
            redisProvider.set(RedisKey.REPLY_START_TIME, replyStartTime, -1);
        }
        return new Result(true, StatusCodeEnum.OK);
    }

    @GetMapping("/get")
    public Result getTime() {
        String enrollEndTime = redisProvider.get(RedisKey.ENROLL_END_TIME);
        String uploadStartTime = redisProvider.get(RedisKey.UPLOAD_START_TIME);
        String uploadEndTime = redisProvider.get(RedisKey.UPLOAD_END_TIME);
        String replyStartTime = redisProvider.get(RedisKey.REPLY_START_TIME);

        TimeConfigVO enrollEndTimeVO = new TimeConfigVO();
        enrollEndTimeVO.setName("报名截止时间");
        enrollEndTimeVO.setValue(enrollEndTime);
        enrollEndTimeVO.setKey("enrollEndTime");

        TimeConfigVO uploadStartTimeVO = new TimeConfigVO();
        uploadStartTimeVO.setName("文件上传开始时间");
        uploadStartTimeVO.setValue(uploadStartTime);
        uploadStartTimeVO.setKey("uploadStartTime");

        TimeConfigVO uploadEndTimeVO = new TimeConfigVO();
        uploadEndTimeVO.setName("文件上传截止时间");
        uploadEndTimeVO.setValue(uploadEndTime);
        uploadEndTimeVO.setKey("uploadEndTime");

        TimeConfigVO replyStartTimeVO = new TimeConfigVO();
        replyStartTimeVO.setName("最终答辩时间");
        replyStartTimeVO.setValue(replyStartTime);
        replyStartTimeVO.setKey("replyStartTime");

        List<TimeConfigVO> list = new ArrayList<>();
        list.add(enrollEndTimeVO);
        list.add(uploadStartTimeVO);
        list.add(uploadEndTimeVO);
        list.add(replyStartTimeVO);
        return new Result(true, StatusCodeEnum.OK, list);
    }

    @GetMapping("/get/updateVO")
    public Result getUpdateVO() {
        String enrollEndTime = redisProvider.get(RedisKey.ENROLL_END_TIME);
        String uploadStartTime = redisProvider.get(RedisKey.UPLOAD_START_TIME);
        String uploadEndTime = redisProvider.get(RedisKey.UPLOAD_END_TIME);
        String replyStartTime = redisProvider.get(RedisKey.REPLY_START_TIME);
        TimeConfigUpdateVO vo = new TimeConfigUpdateVO(enrollEndTime, uploadStartTime, uploadEndTime, replyStartTime);
        return new Result(true, StatusCodeEnum.OK, vo);
    }

}
