package hhtc.jsjds.controller;


import hhtc.jsjds.entity.Academy;
import hhtc.jsjds.entity.Student;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.exception.XscoderException;
import hhtc.jsjds.service.AcademyService;
import hhtc.jsjds.service.StudentService;
import hhtc.jsjds.utils.JwtCommonUtils;
import hhtc.jsjds.vo.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.logging.Handler;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    AcademyService academyService;

    @Autowired
    StudentService studentService;


    @GetMapping("/academylike")
    public Result getPrivince(@RequestParam(required = true) String name){
        return new Result(true, StatusCodeEnum.OK,academyService.queryAcademyByName("%"+name+"%"));
    }

    @PutMapping("/student")
    public Result getPrivince(HttpServletRequest request,String realname, String academy_id, String major_id,
                              String phone, String qq, String imagestring, String id_number, String mail){

        if(StringUtils.isBlank(academy_id)||StringUtils.isBlank(major_id)){
            throw new XscoderException(StatusCodeEnum.BAD_REQUEST);
        }
        String uid = JwtCommonUtils.getIDFromToken(request);
        Student student = new Student();
        student.setStu_name(realname);
        student.setAcademy_id(academy_id);
        student.setMajor_id(major_id);
        student.setStu_phone(phone);
        student.setStu_qq(qq);
        student.setStu_idCard(imagestring);
        student.setId_number(id_number);
        student.setStu_mail(mail);
        student.setUid(uid);
        if(studentService.updateStudent(student)){
            return new Result(true, StatusCodeEnum.OK);
        }else{
            throw new XscoderException(StatusCodeEnum.OPT_FAILD);
        }
    }

    @GetMapping("/student")
    public Result getStudentByToken(HttpServletRequest request){
        String uid = JwtCommonUtils.getIDFromToken(request);
        if(StringUtils.isBlank(uid)){
            throw new XscoderException(StatusCodeEnum.BAD_REQUEST);
        }
        return new Result(true, StatusCodeEnum.OK,studentService.getStudentByUid(uid));
    }



}
