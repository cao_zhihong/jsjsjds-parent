package hhtc.jsjds.redis;


import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisProvider.class);
    @Autowired
    private RedisTemplate redisTemplate;
    @Resource(
            name = "redisTemplate"
    )
    private ValueOperations<String, String> valueOperations;
    @Resource(
            name = "redisTemplate"
    )
    private HashOperations<String, String, Object> hashOperations;
    @Resource(
            name = "redisTemplate"
    )
    private ListOperations<String, Object> listOperations;
    @Resource(
            name = "redisTemplate"
    )
    private SetOperations<String, Object> setOperations;
    @Resource(
            name = "redisTemplate"
    )
    private ZSetOperations<String, Object> zSetOperations;
    public static final long DEFAULT_EXPIRE = 86400L;
    public static final long WEEK_EXPIRE = 604800L;
    public static final long NOT_EXPIRE = -1L;

    public RedisProvider() {
    }

    public void expire(String key, Long expire) {
        try {
            if (StringUtils.isNotBlank(key)) {
                if (expire == null) {
                    expire = -1L;
                }

                this.redisTemplate.expire(key, expire, TimeUnit.SECONDS);
            }
        } catch (Exception var4) {
            LOGGER.error("Redis服务出错:{}", var4.getMessage());
            var4.printStackTrace();
        }

    }

    public void setList(String key, List value, long expire) {
        try {
            int i = 0;

            for (int len = value.size(); i < len; ++i) {
                this.listOperations.rightPush(key, value.get(i));
            }

            if (expire != -1L) {
                this.redisTemplate.expire(key, expire, TimeUnit.SECONDS);
            }
        } catch (Exception var7) {
            LOGGER.error("Redis服务出错:{}", var7.getMessage());
            var7.printStackTrace();
        }

    }

    public void setList(String key, List value) {
        this.setList(key, value, 86400L);
    }

    public <T> List<T> getList(String key, Class<T> clazz) {
        try {
            return this.listOperations.size(key) > 0L ? (List<T>) this.listOperations.range(key, 0L, -1L) : null;
        } catch (Exception var4) {
            LOGGER.error("Redis服务出错:{}", var4.getMessage());
            var4.printStackTrace();
            return null;
        }
    }

    public void set(String key, Object value, long expire) {
        try {
            this.valueOperations.set(key, this.toJson(value));
            if (expire != -1L) {
                this.redisTemplate.expire(key, expire, TimeUnit.SECONDS);
            }
        } catch (Exception var6) {
            LOGGER.error("Redis服务出错:{}", var6.getMessage());
            var6.printStackTrace();
        }

    }

    public Long incr(String key) {
        try {
            return this.valueOperations.increment(key, 1L);
        } catch (Exception var3) {
            LOGGER.error("Redis服务出错:{}", var3.getMessage());
            var3.printStackTrace();
            return 0L;
        }
    }

    public Long incr(String key, Long delta) {
        try {
            if (delta == null) {
                delta = 1L;
            }

            return this.valueOperations.increment(key, delta);
        } catch (Exception var4) {
            LOGGER.error("Redis服务出错:{}", var4.getMessage());
            var4.printStackTrace();
            return 0L;
        }
    }

    public void set(String key, Object value) {
        this.set(key, value, 86400L);
    }

    public <T> T get(String key, Class<T> clazz, long expire) {
        try {
            String value = (String) this.valueOperations.get(key);
            if (expire != -1L) {
                this.redisTemplate.expire(key, expire, TimeUnit.SECONDS);
            }

            return value == null ? null : this.fromJson(value, clazz);
        } catch (Exception var6) {
            LOGGER.error("Redis服务出错:{}", var6.getMessage());
            var6.printStackTrace();
            return null;
        }
    }

    public <T> T get(String key, Class<T> clazz) {
        return this.get(key, clazz, -1L);
    }

    public <T> T getNotSetExp(String key, Class<T> clazz) {
        try {
            String value = (String) this.valueOperations.get(key);
            return value == null ? null : this.fromJson(value, clazz);
        } catch (Exception var4) {
            LOGGER.error("Redis服务出错:{}", var4.getMessage());
            var4.printStackTrace();
            return null;
        }
    }

    public String get(String key, long expire) {
        try {
            String value = (String) this.valueOperations.get(key);
            if (expire != -1L) {
                this.redisTemplate.expire(key, expire, TimeUnit.SECONDS);
            }

            return value;
        } catch (Exception var5) {
            LOGGER.error("Redis服务出错:{}", var5.getMessage());
            var5.printStackTrace();
            return null;
        }
    }

    public String get(String key) {
        return this.get(key, -1L);
    }

    public String getNotSetExp(String key) {
        try {
            return (String) this.valueOperations.get(key);
        } catch (Exception var3) {
            LOGGER.error("Redis服务出错:{}", var3.getMessage());
            var3.printStackTrace();
            return null;
        }
    }

    public void delete(String key) {
        try {
            this.redisTemplate.delete(key);
        } catch (Exception var3) {
            LOGGER.error("Redis服务出错:{}", var3.getMessage());
            var3.printStackTrace();
        }

    }

    public void deleteByPrex(String prex) {
        try {
            Set<String> keys = this.redisTemplate.keys(prex);
            if (CollectionUtils.isNotEmpty(keys)) {
                this.redisTemplate.delete(keys);
            }
        } catch (Exception var3) {
            LOGGER.error("Redis服务出错:{}", var3.getMessage());
            var3.printStackTrace();
        }

    }

    public boolean zrevAdd(String key, Object value, double score) {
        try {
            return this.zSetOperations.add(key, value, score);
        } catch (Exception var6) {
            LOGGER.error("Redis服务出错:{}", var6.getMessage());
            var6.printStackTrace();
            return false;
        }
    }

    public Set<Object> zrevRangeGap(String key, Long start, Long end) {
        try {
            return this.zSetOperations.range(key, start, end);
        } catch (Exception var5) {
            LOGGER.error("Redis服务出错:{}", var5.getMessage());
            var5.printStackTrace();
            return null;
        }
    }

    public Set<Object> zrevReverseRangeGap(String key, Long start, Long end) {
        try {
            return this.zSetOperations.reverseRange(key, start, end);
        } catch (Exception var5) {
            LOGGER.error("Redis服务出错:{}", var5.getMessage());
            var5.printStackTrace();
            return null;
        }
    }

    public Set<Object> zrevRange(String key) {
        try {
            return this.zSetOperations.range(key, 0L, -1L);
        } catch (Exception var3) {
            LOGGER.error("Redis服务出错:{}", var3.getMessage());
            var3.printStackTrace();
            return null;
        }
    }

    public Long zrevRemove(String key, Object... values) {
        try {
            return this.zSetOperations.remove(key, values);
        } catch (Exception var4) {
            LOGGER.error("Redis服务出错:{}", var4.getMessage());
            var4.printStackTrace();
            return null;
        }
    }

    public Long zrevSize(String key) {
        try {
            return this.zSetOperations.size(key);
        } catch (Exception var3) {
            LOGGER.error("Redis服务出错:{}", var3.getMessage());
            var3.printStackTrace();
            return 0L;
        }
    }

    public Long zrevRank(String key, Object o) {
        try {
            return this.zSetOperations.rank(key, o);
        } catch (Exception var4) {
            LOGGER.error("Redis服务出错:{}", var4.getMessage());
            var4.printStackTrace();
            return null;
        }
    }

    public Set<String> keys(String key) {
        try {
            return this.redisTemplate.keys(key);
        } catch (Exception var3) {
            LOGGER.error("Redis服务出错:{}", var3.getMessage());
            var3.printStackTrace();
            return null;
        }
    }

    private String toJson(Object object) {
        return !(object instanceof Integer) && !(object instanceof Long) && !(object instanceof Float) && !(object instanceof Double) && !(object instanceof Boolean) && !(object instanceof String) ? JSON.toJSONString(object) : String.valueOf(object);
    }

    private <T> T fromJson(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }
}
