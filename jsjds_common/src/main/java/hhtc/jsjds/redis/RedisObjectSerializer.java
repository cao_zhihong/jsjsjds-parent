package hhtc.jsjds.redis;


import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.data.redis.serializer.RedisSerializer;

public class RedisObjectSerializer implements RedisSerializer<Object> {
    private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
    private final Converter<Object, byte[]> serializingConverter = new SerializingConverter();
    private final Converter<byte[], Object> deserializingConverter = new DeserializingConverter();

    public RedisObjectSerializer() {
    }

    @Override
    public byte[] serialize(Object obj) {
        return obj == null ? EMPTY_BYTE_ARRAY : (byte[]) this.serializingConverter.convert(obj);
    }

    @Override
    public Object deserialize(byte[] data) {
        return data != null && data.length != 0 ? this.deserializingConverter.convert(data) : null;
    }
}
