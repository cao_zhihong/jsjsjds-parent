package hhtc.jsjds.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;


@Configuration
@EnableAsync
public class TaskPoolConfig {

    /**
     * io密集型
     *
     * @return
     */
    @Bean("taskExecutor")
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(10);   //核心线程池大小
        taskExecutor.setMaxPoolSize(20);    //线程池中最大线程数
        taskExecutor.setQueueCapacity(10);     //队列等待数
        taskExecutor.setKeepAliveSeconds(60);   //线程数大于核心时，多于的空闲线程最多存活时间
        taskExecutor.setThreadNamePrefix("taskExecutor--");     //线程名前缀
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setAwaitTerminationSeconds(60);
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());    //饱和策略，超过最大线程数时，退回给主线程run
        return taskExecutor;
    }


}
