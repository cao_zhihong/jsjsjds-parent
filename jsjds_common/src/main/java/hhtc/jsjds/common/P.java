package hhtc.jsjds.common;



import java.io.Serializable;
import java.util.List;

public class P implements Serializable {
    private static final long serialVersionUID = 1L;
    private long totalCount;
    private int pageSize;
    private long totalPage;
    private int currPage;
    private List<?> list;
    private boolean hasNext = false;

    public P() {
    }

    public P(List<?> list, long totalCount, int pageSize, int currPage) {
        this.list = list;
        this.totalCount = totalCount;
        this.pageSize = pageSize;
        this.currPage = currPage;
        this.totalPage = (long) Math.ceil((double) totalCount / (double) pageSize);
    }

    public P(List<?> list, long totalCount, int pageSize, int currPage, long totalPage) {
        this.list = list;
        this.totalCount = totalCount;
        this.pageSize = pageSize;
        this.currPage = currPage;
        this.totalPage = totalPage;
    }

    public long getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotalPage() {
        return this.totalPage;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrPage() {
        return this.currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public List<?> getList() {
        return this.list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    public boolean isHasNext() {
        return this.getTotalPage() > (long) this.getCurrPage() ? true : this.hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }
}
