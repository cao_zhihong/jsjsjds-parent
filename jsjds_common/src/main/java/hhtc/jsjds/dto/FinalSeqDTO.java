package hhtc.jsjds.dto;

import lombok.Data;

@Data
public class FinalSeqDTO {

    private Integer categoryId;

    private String areaName;

    private Integer teamId;
    private Integer areaId;

}
