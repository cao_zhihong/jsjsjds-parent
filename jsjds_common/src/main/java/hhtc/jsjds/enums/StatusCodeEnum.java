package hhtc.jsjds.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum StatusCodeEnum {
    SMS_FAIL(499999, "验证码发送失败，请联系管理员"),
    OK(50000, "成功"),

    STUDENT_NOT_FOUND(50001, "数据库中不存在此学号"),
    BAD_REQUEST(50002, "请求参数有误"),
    WRONG_CODE(50003, "短信验证码有误"),
    REGISTER_FAILED(50004, "注册失败，请联系管理员"),
    INVALID_FILE_TYPE(50005, "错误的文件类型"),
    NO_RECORD(50006, "此教师未保存"),
    SMSOK(50007, "短信验证码发送成功"),
    REGISTER_SUCCESS(50008, "注册成功"),
    OPT_TOO_FAST(50009,"操作频繁"),
    OPT_FAILD(500010,"操作失败"),
    STUDENT_NOT_EXIST(500011,"学生不存在，请等待教师导入学校名单"),
    STUDENT_HAS_BINDED(500012,"该学生已绑定，如非本人操作，请联系任课教师"),
    STUDENT_NOT_BINDED(500013,"该学生尚未绑定"),
    WX_LOGIN_FAILED(500014,"微信登录失败"),
    STUDENT_WAIT_BIND_PHONE(500015,"尚未绑定手机号"),
    TOKEN_NOT_CORRECT(500016,"token参数有误"),
    NOT_WORKTIME(500017,"非授课时间"),
    COURSE_CONFLICT(500018,"课程出现冲突，请联系任课老师"),
    CLASS_CREATE_FAILED(500019,"添加班级失败"),
    WRONG_VCODE(500020,"验证码错误"),
    STUDENT_HAS_REGISTED(500021,"该学生已注册，如非本人操作，请联系工作人员"),
    MAJOR_NOT_SATISFIED(500022,"您所学专业属于专业组类别，无法参加普通组队伍"),
    COUNT_LIMIT_ERROR(500023,"每位同学在同一大类中仅允许参加一支队伍"),
    WRONG_INV_CODE(500024,"队伍邀请码错误"),
    COUNT_TOTALLIMIT_ERROR(500025,"每位同学最多参加两个队伍"),
    TEAM_HAS_FULL(500026,"加入失败，该队伍人数已满"),
    INFO_NOT_COMPLETED(500027,"失败，您的个人信息尚未补充完整"),
    WRONG_USERNAME_OR_PASSWORD(500028,"用户名或密码错误"),
    PARSE_TIME_FAILED(500029,"'截至时间'转换错误，请联系工作人员"),
    ENROLL_HAS_ENDED(500030,"报名已截至"),
    MAJOR_HAS_LIMITED(500031,"该类别只允许该专业同学报名"),
    EXIT_HAS_FAILED(500032, "退出失败，请联系工作人员"),
    CHANGE_NOT_SATISFIED(500034, "人数不足三人，队伍无需调整"),
    CHANGE_HAS_FAILED(500035, "调整失败，请联系工作人员"),
    REGIST_NOT_ALLOWED(500036, "该字段不允许注册"),
    CHANGE_NOT_ALLOWED(500037, "报名阶段结束后，不再支持队伍操作，如需调整，请联系工作人员"),
    OPT_NOT_ALLOWED(500038, "此操作不被允许，请联系工作人员"),
    UPLOAD_HAS_ENDED(500039, "上传作品已截至"),
    UPLOAD_HAS_NOT_START(500040, "上传作品还未开始"),
    TEAM_NOT_FOUND(500041, "未找到此队伍"),
    RATE_HAS_FINISHED(500042, "您已对此队伍评分，如需修改，请移至'已完成'"),
    COUNT_ERROR(500043, "目前仅支持对三人队伍进行调整，若需修改，请尝试通知队员退出队伍后重新加入。"),
    PHONE_NOT_FORMAT(500044, "手机号码不合法"),
    SMS_EXCEED(500045, "今日验证码发送已达上限"),
    EXCEL_EXPORT_FAIL(500046, "Excel导出失败,请联系管理员"),
    INSERT_FAIL(500047, "插入失败，请查看数据是否符合规则要求"),
    EXCEL_UPLOAD_FAIL(500048, "上传数据失败，请联系管理员"),
    GROUP_IS_EXIT(500049, "生成答辩顺序失败，原因:已经有答辩记录存在，请删除后再试"),
    GROUP_FAIL(500050, "生成答辩顺序失败，请联系管理员查看"),
    AREA_NOT_COMPLETE(500051, "生成答辩顺序失败，原因：答辩场地未补充完整"),
    SMS_SUCCESS(500000, "验证码发送成功");
    private int code;
    private String message;

}
