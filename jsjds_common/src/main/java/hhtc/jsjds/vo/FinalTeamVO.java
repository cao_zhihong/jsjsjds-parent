package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/27 15:46
 */
@Data
public class FinalTeamVO {

    private Integer id;
    private Integer teamId;
    private String CategoryRootName;
    private String CategoryChildName;
    private String WorksName;
}
