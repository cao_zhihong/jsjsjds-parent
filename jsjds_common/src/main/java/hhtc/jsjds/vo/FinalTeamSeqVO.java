package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/28 13:10
 */
@Data
public class FinalTeamSeqVO {
    private Integer teamId;

    private String areaName;

    private String sequence;

    private String worksName;

    private String categoryRootName;

    private String categoryChildName;
}
