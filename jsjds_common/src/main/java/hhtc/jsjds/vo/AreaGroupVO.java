package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/29 12:13
 */
@Data
public class AreaGroupVO {

    private String groupCategoryId;
    private String groupId;
    private String areaName;

}
