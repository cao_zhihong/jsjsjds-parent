package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/28 15:39
 */
@Data
public class NotifyStudentSmsVO {

    private String stuName;

    private String worksName;

    private String stuPhone;

    private String sequence;

    private String areaName;

}
