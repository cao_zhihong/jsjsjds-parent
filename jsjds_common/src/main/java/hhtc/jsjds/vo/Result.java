package hhtc.jsjds.vo;

import hhtc.jsjds.enums.StatusCodeEnum;
import lombok.Data;

@Data
public class Result {
    private boolean flag;
    private Integer code;
    private String message;
    private Object data;

    public Result(boolean flag, Integer code, String message) {
        this.flag = flag;
        this.code = code;
        this.message = message;
    }

    public Result(boolean flag, Integer code, String message, Object data) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(boolean flag, StatusCodeEnum statusCodeEnum) {
        this.flag = flag;
        this.code = statusCodeEnum.getCode();
        this.message = statusCodeEnum.getMessage();
    }

    public Result(boolean flag, StatusCodeEnum statusCodeEnum, Object data) {
        this.flag = flag;
        this.code = statusCodeEnum.getCode();
        this.message = statusCodeEnum.getMessage();
        this.data = data;
    }

    public Result() {
    }
}
