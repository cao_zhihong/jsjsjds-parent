package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/22 20:03
 */
@Data
public class FilterConfigVO {
    private Integer id;
    private String academyName;
    private String categoryName;

}
