package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/25 21:25
 */
@Data
public class AreaVO {
    private Integer id;
    private String areaName;
    private String categoryName;

}
