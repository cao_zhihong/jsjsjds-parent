package hhtc.jsjds.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/29 17:12
 */
@Data
public class FinalSeqExcelVO {

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(15)//单元格宽度
    @ExcelProperty("队伍编号")//标题名称
    private Integer teamId;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(30)//单元格宽度
    @ExcelProperty("作品名称")//标题名称
    private String worksName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(30)//单元格宽度
    @ExcelProperty("作品分类(大类)")//标题名称
    private String categoryRootName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(50)//单元格宽度
    @ExcelProperty("作品分类(小类)")//标题名称
    private String categoryChildName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(30)//单元格宽度
    @ExcelProperty("指导老师")//标题名称
    private String teacherName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(30)//单元格宽度
    @ExcelProperty("学生姓名")//标题名称
    private String stuName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(30)//单元格宽度
    @ExcelProperty("答辩场地名称")//标题名称
    private String areaName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(15)//单元格宽度
    @ExcelProperty("答辩顺序")//标题名称
    private String sequence;


}
