package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/30 14:09
 */
@Data
public class TimeConfigUpdateVO {
    private String enrollEndTime;
    private String uploadStartTime;
    private String uploadEndTime;
    private String replyStartTime;

    public TimeConfigUpdateVO(String enrollEndTime, String uploadStartTime, String uploadEndTime, String replyStartTime) {
        this.enrollEndTime = enrollEndTime;
        this.uploadStartTime = uploadStartTime;
        this.uploadEndTime = uploadEndTime;
        this.replyStartTime = replyStartTime;
    }

    public TimeConfigUpdateVO() {
    }
}
