package hhtc.jsjds.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/4/2 13:46
 */
@Data
public class FinalScoreVO {


    @ExcelIgnore
    private Integer id;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(15)//单元格宽度
    @ExcelProperty("队伍编号")//标题名称
    private Integer teamId;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ColumnWidth(30)
    @ExcelProperty("作品类别(大类)")
    private String categoryRootName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ColumnWidth(50)
    @ExcelProperty("作品类别(小类)")
    private String categoryChildName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ColumnWidth(30)
    @ExcelProperty("作品名称")
    private String worksName;


    @ExcelIgnore
    private String worksUrl;

    @ExcelIgnore
    private String worksKey;


    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ColumnWidth(20)
    @ExcelProperty("指导老师")
    private String teacherName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ColumnWidth(50)
    @ExcelProperty("作者：第一作者、第二作者、第三作者（顺序）")
    private String stuName;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ColumnWidth(10)
    @ExcelProperty("平均分")
    private Double avgScore;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
    @ColumnWidth(20)
    @ExcelProperty("评委分数")
    private String allScore;


}
