package hhtc.jsjds.vo;

import lombok.Data;

/**
 * @author ZhangYi zhangyi-time@foxmail.com
 * @date 2021/3/29 21:23
 */
@Data
public class TimeConfigVO {

    private String name;

    private String key;
    private String value;
}
