package hhtc.jsjds.utils;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@ToString
@Component
public class JwtCommonConfig {

    @Value("${xscoder.security.jwt.url:/login}")
    private String url;

    @Value("${xscoder.security.jwt.header:Authorization}")
    private String header;

    @Value("${xscoder.security.jwt.prefix:Bearer}")
    private String prefix;

    @Value("${xscoder.security.jwt.expiration:#{24*60*60}}")
    private int expiration; // default 24 hours

    @Value("${xscoder.security.jwt.secret:otherpeopledontknowit}")
    private String secret;
}
