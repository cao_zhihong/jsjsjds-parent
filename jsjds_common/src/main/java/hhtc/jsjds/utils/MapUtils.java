package hhtc.jsjds.utils;

/**
 * @author ZhangYi
 * @email zhangyi-time@foxmail.com
 * @date 2020/8/3 16:16
 */

import org.apache.commons.lang3.StringUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


public class MapUtils extends HashMap<String, Object> {

    @Override
    public MapUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public static TreeMap bean2TreeMap(Object javaBean) {
        TreeMap map = new TreeMap();

        try {
            // 获取javaBean属性
            BeanInfo beanInfo = Introspector.getBeanInfo(javaBean.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            if (propertyDescriptors != null && propertyDescriptors.length > 0) {
                String propertyName = null; // javaBean属性名
                Object propertyValue = null; // javaBean属性值
                for (PropertyDescriptor pd : propertyDescriptors) {
                    propertyName = pd.getName();
                    if (!propertyName.equals("class")) {
                        Method readMethod = pd.getReadMethod();
                        propertyValue = readMethod.invoke(javaBean, new Object[0]);
                        map.put(propertyName, propertyValue);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }


    /**
     * map按assci排序后转为window.location.search参数值,值为null|""的参数忽略
     *
     * @param paramMap
     * @return
     */
    public static String mapSortSearch(Map<String, Object> paramMap) {
        StringBuilder rawSignature = new StringBuilder();
        Iterator<Entry<String, Object>> it = paramMap.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = it.next();
            String value = null;
            if (entry.getValue() != null) {
                value = entry.getValue().toString();
                if (StringUtils.isNotBlank(value)) {
                    rawSignature.append(entry.getKey()).append("=").append(value).append("&");
                }
            }
        }
        return rawSignature.deleteCharAt(rawSignature.length() - 1).toString();
    }


    public static String mapString(Map<String, Object> paramMap, String sign) {
        return new StringBuilder(mapSortSearch(paramMap)).append("&sign=").append(sign).toString();
    }


    /**
     * map按assci排序后转为window.location.search参数值
     *
     * @param paramMap
     * @return
     */
    public static String mapSort2Search(Map<String, Object> paramMap) {
        StringBuilder rawSignature = new StringBuilder();
        Iterator<Entry<String, Object>> it = paramMap.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = it.next();
            String value = null;
            value = entry.getValue().toString();
            rawSignature.append(entry.getKey()).append("=").append(value).append("&");
        }
        return rawSignature.deleteCharAt(rawSignature.length() - 1).toString();
    }


    public static String map2String(Map<String, Object> paramMap, String sign) {
        return new StringBuilder(mapSort2Search(paramMap)).append("&sign=").append(sign).toString();
    }


    /**
     * treeMap按assci排序后转为window.location.search参数值,值为null|""的参数忽略
     *
     * @param paramMap
     * @return
     */
    public static String treeMapSortSearch(TreeMap<String, Object> paramMap) {
        StringBuilder rawSignature = new StringBuilder();
        Iterator<Entry<String, Object>> it = paramMap.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = it.next();
            String value = null;
            if (entry.getValue() != null) {
                value = entry.getValue().toString();
                if (StringUtils.isNotBlank(value)) {
                    rawSignature.append(entry.getKey()).append("=").append(value).append("&");
                }
            }
        }
        return rawSignature.deleteCharAt(rawSignature.length() - 1).toString();
    }


    public static String treeMapString(TreeMap<String, Object> paramMap, String signChar, String salt) {
        signChar = StringUtils.isBlank(signChar) ? "secret_key" : signChar;
        return new StringBuilder(treeMapSortSearch(paramMap)).append("&").append(signChar).append("=").append(salt).toString();
    }
}
