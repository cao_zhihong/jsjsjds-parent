package hhtc.jsjds.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

@Component
public class JwtCommonUtils {

    private static JwtCommonConfig config;

    private static JwtCommonUtils jwtCommonUtils;

    @PostConstruct
    public void init(){
        jwtCommonUtils = this;
        jwtCommonUtils.config = this.config;
    }

    @Autowired
    public void setConfig( JwtCommonConfig config){
        jwtCommonUtils.config = config;
    }
    public JwtCommonConfig getConfig(){
        return config;
    }

    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    private static Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(config.getSecret().getBytes()).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    /**
     * 从令牌中获取用户名
     *
     * @param token 令牌
     * @return 用户名
     */
    public static String getUsernameFromToken(String token) {
        String username;
        try {
            Claims claims = getClaimsFromToken(token.trim());
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    /**
     * 从令牌中获取学生id
     *
     * @param httpServletRequest 请求
     * @return 用户名
     */
    public static String getStudentIDFromToken(HttpServletRequest httpServletRequest) {
        String authHeader = httpServletRequest.getHeader("Authorization");
        if (authHeader != null && authHeader.startsWith(config.getPrefix())) {
            String authToken = authHeader.replace(config.getPrefix() + " ", "");
            if (validateToken(authToken)) {
                String stu_id;
                try {
                    Claims claims = getClaimsFromToken(authToken.trim());
                    stu_id = claims.getSubject();
                } catch (Exception e) {
                    stu_id = null;
                }
                return stu_id;
            }
            return null;
        }
        return null;
    }


    /**
     * 从令牌中获取教师id
     *
     * @param httpServletRequest 请求
     * @return 用户名
     */
    public static String getIDFromToken(HttpServletRequest httpServletRequest) {
        String authHeader = httpServletRequest.getHeader("Authorization");
        if (authHeader != null && authHeader.startsWith(config.getPrefix())) {
            String authToken = authHeader.replace(config.getPrefix() + " ", "");
            if (validateToken(authToken)) {
                String tch_id;
                try {
                    Claims claims = getClaimsFromToken(authToken.trim());
                    tch_id = claims.get("id", String.class);
                } catch (Exception e) {
                    tch_id = null;
                }
                return tch_id;
            }
            return null;
        }
        return null;
    }


    /**
     * 判断是否有效
     * @param authToken
     * @return
     */
    public static Boolean validateToken(String authToken) {
//        Object o = redis.opsForValue().get(authToken);
//        if(null != o){
//            return true;
//        }
//        return false;
        return true;
    }

}
