package hhtc.jsjds.param;

import lombok.Data;


@Data
public class AreaInsertParam {

    private Integer[] ids;

    private String areaName;


}
