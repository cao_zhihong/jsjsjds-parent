package hhtc.jsjds.constant;


public class RedisKey {

    public static final String PROJECT = "jsjsjds:";
    public static final String USER = PROJECT + "user:";
    public static final String VERIFICATION = USER + "verification:";
    public static final String VERIFICATION_COUNT = USER + "verification:count:";

    public static final String TIME = PROJECT + "time:";
    public static final String ENROLL_END_TIME = TIME + "enroll_end_time";
    public static final String UPLOAD_START_TIME = TIME + "upload_start_time";
    public static final String UPLOAD_END_TIME = TIME + "upload_end_time";
    public static final String REPLY_START_TIME = TIME + "reply_end_time";


}
