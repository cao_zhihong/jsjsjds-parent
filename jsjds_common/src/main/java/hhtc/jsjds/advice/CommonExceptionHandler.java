package hhtc.jsjds.advice;

import hhtc.jsjds.exception.XscoderException;
import hhtc.jsjds.vo.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@CrossOrigin
@ResponseBody
public class CommonExceptionHandler {

    @ExceptionHandler(XscoderException.class)
    public Result handleException(XscoderException e){
        return  new Result(false,e.getStatusCodeEnum().getCode(),e.getStatusCodeEnum().getMessage());
    }

}
