package hhtc.jsjds.exception;

import hhtc.jsjds.enums.StatusCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class XscoderException extends RuntimeException{
    private StatusCodeEnum statusCodeEnum;
}
