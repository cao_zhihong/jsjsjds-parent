package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.dto.FinalSeqDTO;
import hhtc.jsjds.entity.FinalSeqPojo;
import hhtc.jsjds.vo.FinalSeqExcelVO;
import hhtc.jsjds.vo.FinalTeamSeqVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 最终答辩顺序(FinalSeq)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-26 17:53:54
 */
public interface FinalSeqDao extends BaseMapper<FinalSeqPojo> {

    List<FinalSeqDTO> getFinalSeqGroup();

    List<FinalSeqPojo> getFinalSeqGroupById(@Param("id") Integer id, @Param("categoryId") Integer categoryId);

    List<FinalTeamSeqVO> getAll(@Param("page") Page<FinalTeamSeqVO> page, @Param("search") Map<String, Object> search);

    void removeAll();

    List<FinalSeqExcelVO> getExportExcelMsg(@Param("idList") List<String> idList);
}
