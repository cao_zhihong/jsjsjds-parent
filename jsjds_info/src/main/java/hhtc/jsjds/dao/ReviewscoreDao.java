package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hhtc.jsjds.entity.ReviewscorePojo;
import hhtc.jsjds.vo.FirstReviewScoreVO;

import java.util.List;

/**
 * (Reviewscore)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-24 15:49:40
 */
public interface ReviewscoreDao extends BaseMapper<ReviewscorePojo> {

    List<FirstReviewScoreVO> exportFirstReviewScore();
}
