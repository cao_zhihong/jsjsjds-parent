package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hhtc.jsjds.entity.CategoryPojo;

/**
 * (Category)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-22 19:41:56
 */
public interface CategoryDao extends BaseMapper<CategoryPojo> {

}
