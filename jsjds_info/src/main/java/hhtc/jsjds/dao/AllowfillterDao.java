package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.AllowfillterPojo;
import hhtc.jsjds.vo.FilterConfigVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Allowfillter)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-21 20:48:54
 */
@Mapper
public interface AllowfillterDao extends BaseMapper<AllowfillterPojo> {

    List<FilterConfigVO> getAllowfillter(Page<FilterConfigVO> p);
}
