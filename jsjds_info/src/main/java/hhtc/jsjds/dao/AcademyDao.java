package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hhtc.jsjds.entity.AcademyPojo;

/**
 * (Academy)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-21 20:22:06
 */
public interface AcademyDao extends BaseMapper<AcademyPojo> {

}
