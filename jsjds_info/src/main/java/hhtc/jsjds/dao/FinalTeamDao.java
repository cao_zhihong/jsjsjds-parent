package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.FinalTeamPojo;
import hhtc.jsjds.vo.FinalTeamVO;
import hhtc.jsjds.vo.NotifyStudentSmsVO;

import java.util.List;

/**
 * 最终参与答辩的队伍(FinalTeam)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-26 18:12:09
 */
public interface FinalTeamDao extends BaseMapper<FinalTeamPojo> {

    List<FinalTeamVO> getAll(Page<FinalTeamVO> page);

    void removeAll();

    List<NotifyStudentSmsVO> getNotifyMsg();
}
