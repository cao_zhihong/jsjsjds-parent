package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hhtc.jsjds.entity.TeamPojo;

/**
 * (Team)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-27 20:01:32
 */
public interface TeamDao extends BaseMapper<TeamPojo> {

}
