package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.FillterPojo;
import hhtc.jsjds.vo.FilterConfigVO;

import java.util.List;

/**
 * 哪些专业不能报名非专业组(Fillter)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-30 15:14:34
 */
public interface FillterDao extends BaseMapper<FillterPojo> {

    List<FilterConfigVO> getAllFilter(Page<FilterConfigVO> page);
}
