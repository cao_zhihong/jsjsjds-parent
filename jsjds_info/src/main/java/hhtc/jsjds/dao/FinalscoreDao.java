package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.FinalScorePojo;
import hhtc.jsjds.vo.FinalScoreVO;

import java.util.List;

/**
 * (Finalscore)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-02 14:03:18
 */
public interface FinalscoreDao extends BaseMapper<FinalScorePojo> {

    List<FinalScoreVO> getFinalScore();

    List<FinalScoreVO> getAll(Page<FinalScoreVO> page);
}
