package hhtc.jsjds.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.AreaPojo;
import hhtc.jsjds.vo.AreaGroupVO;
import hhtc.jsjds.vo.AreaVO;

import java.util.List;

/**
 * 答辩厂分配表(Area)表数据库访问层
 *
 * @author makejava
 * @since 2021-03-25 21:11:22
 */
public interface AreaDao extends BaseMapper<AreaPojo> {

    List<AreaVO> getAllArea(Page<AreaVO> page);

    List<AreaGroupVO> getGroupConcatIdAndCategoryId();
}
