package hhtc.jsjds.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.AreaPojo;
import hhtc.jsjds.entity.FinalSeqPojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.exception.XscoderException;
import hhtc.jsjds.param.AreaInsertParam;
import hhtc.jsjds.service.AreaService;
import hhtc.jsjds.service.FinalSeqService;
import hhtc.jsjds.vo.AreaVO;
import hhtc.jsjds.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 答辩厂分配表(Area)表控制层
 *
 */
@RestController
@RequestMapping("area")
public class AreaController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private AreaService areaService;

    @Autowired
    private FinalSeqService finalSeqService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<AreaVO> page) {
        Page<AreaVO> allArea = areaService.getAllArea(page);
        return success(allArea);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.areaService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param areaPojo 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody AreaPojo areaPojo) {
        return success(this.areaService.save(areaPojo));
    }

    /**
     * 修改数据
     *
     * @param areaPojo 实体对象
     * @return 修改结果
     */
    @PutMapping
    public Result update(@RequestBody AreaPojo areaPojo) {
        return new Result(this.areaService.updateById(areaPojo), StatusCodeEnum.OK);
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestBody List<Long> ids) {
        finalSeqService.remove(new QueryWrapper<FinalSeqPojo>().in("area", ids));
        return success(this.areaService.removeByIds(ids));
    }

    /**
     * 批量插入
     *
     * @param area 需要插入的数据
     * @return
     */
    @PostMapping("/batch")
    public Result insertBatch(@RequestBody AreaInsertParam area) {
        try {
            List<AreaPojo> list = new ArrayList<>();
            for (Integer id : area.getIds()) {
                AreaPojo areaDTO = new AreaPojo();
                areaDTO.setAreaName(area.getAreaName());
                areaDTO.setCategoryId(id);
                list.add(areaDTO);
            }
            areaService.saveBatch(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new XscoderException(StatusCodeEnum.INSERT_FAIL);
        }
        return new Result(true, StatusCodeEnum.OK);
    }

    /**
     * 获取已经配置场地信息的分类
     *
     * @return
     */
    @GetMapping("/existCategory")
    public Result getExistCategory() {
        List<AreaPojo> CategoryIds = areaService.list(new QueryWrapper<AreaPojo>().select("category_id"));
        List<Integer> result = new ArrayList<>();
        for (AreaPojo categoryId : CategoryIds) {
            result.add(categoryId.getCategoryId());
        }
        return new Result(true, StatusCodeEnum.OK, result);
    }
}
