package hhtc.jsjds.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.AllowfillterPojo;
import hhtc.jsjds.entity.CategoryPojo;
import hhtc.jsjds.entity.FillterPojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.AllowfillterService;
import hhtc.jsjds.service.CategoryService;
import hhtc.jsjds.service.FillterService;
import hhtc.jsjds.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (Category)表控制层
 *
 */
@RestController
@RequestMapping("category")
public class CategoryController extends ApiController {

    @Autowired
    private AllowfillterService allowfillterService;

    @Autowired
    private FillterService fillterService;

    /**
     * 服务对象
     */
    @Resource
    private CategoryService categoryService;


    @GetMapping("/all")
    public Result getAll() {
        List<CategoryPojo> result = categoryService.getAllRootCategory();
        return new Result(true, StatusCodeEnum.OK, result);
    }

    /**
     * 分页查询所有数据
     *
     * @param page              分页对象
     * @param categoryPojoEntry 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<CategoryPojo> page, CategoryPojo categoryPojoEntry, Integer parentId) {
        return success(this.categoryService.page(page, new QueryWrapper<>(categoryPojoEntry).eq("parent_id", parentId)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.categoryService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param categoryPojoEntry 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody CategoryPojo categoryPojoEntry) {
        return success(this.categoryService.save(categoryPojoEntry));
    }

    /**
     * 修改数据
     *
     * @param categoryPojoEntry 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody CategoryPojo categoryPojoEntry) {
        return success(this.categoryService.updateById(categoryPojoEntry));
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestBody List<Long> ids) {
        this.categoryService.remove(new QueryWrapper<CategoryPojo>().in("parent_id", ids));
        this.categoryService.removeByIds(ids);
        allowfillterService.remove(new QueryWrapper<AllowfillterPojo>().in("categoryA_id", ids));
        fillterService.remove(new QueryWrapper<FillterPojo>().in("categoryA_id", ids));
        return success(true);
    }
}
