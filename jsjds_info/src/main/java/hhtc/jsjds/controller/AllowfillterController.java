package hhtc.jsjds.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.common.P;
import hhtc.jsjds.entity.AllowfillterPojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.AllowfillterService;
import hhtc.jsjds.vo.FilterConfigVO;
import hhtc.jsjds.vo.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (Allowfillter)表控制层
 *
 */
@RestController
@RequestMapping("allowfillter")
public class AllowfillterController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private AllowfillterService allowfillterService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @GetMapping
    public Result selectAll(P page) {
        Page<FilterConfigVO> p = allowfillterService.getAllowfillter(page);
        return new Result(true, StatusCodeEnum.OK, p);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.allowfillterService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param allowfillterPojo 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody AllowfillterPojo allowfillterPojo) {
        return success(this.allowfillterService.save(allowfillterPojo));
    }

    /**
     * 修改数据
     *
     * @param allowfillterPojo 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody AllowfillterPojo allowfillterPojo) {
        return success(this.allowfillterService.updateById(allowfillterPojo));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestBody List<Long> idList) {
        return success(this.allowfillterService.removeByIds(idList));
    }
}
