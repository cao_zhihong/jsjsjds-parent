package hhtc.jsjds.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.AcademyPojo;
import hhtc.jsjds.entity.AllowfillterPojo;
import hhtc.jsjds.entity.FillterPojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.AcademyService;
import hhtc.jsjds.service.AllowfillterService;
import hhtc.jsjds.service.FillterService;
import hhtc.jsjds.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (Academy)表控制层
 *
 */
@RestController
@RequestMapping("academy")
public class AcademyController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private AcademyService academyService;


    @Autowired
    private AllowfillterService allowfillterService;

    @Autowired
    private FillterService fillterService;

    @GetMapping("/allProfession")
    public Result getAllProfession() {
        List<AcademyPojo> result = academyService.list(new QueryWrapper<AcademyPojo>().ne("parent_id", 0));
        return new Result(true, StatusCodeEnum.OK, result);
    }

    @GetMapping("/allAcademy")
    public Result getAllAcademy() {
        List<AcademyPojo> result = academyService.list(new QueryWrapper<AcademyPojo>().eq("parent_id", 0));
        return new Result(true, StatusCodeEnum.OK, result);
    }

    /**
     * 分页查询所有数据
     *
     * @param page        分页对象
     * @param academyPojo 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<AcademyPojo> page, AcademyPojo academyPojo, Integer parentId) {
        return success(this.academyService.page(page, new QueryWrapper<>(academyPojo).eq("parent_id", parentId)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.academyService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param academyPojo 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody AcademyPojo academyPojo) {
        return success(this.academyService.save(academyPojo));
    }

    /**
     * 修改数据
     *
     * @param academyPojo 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody AcademyPojo academyPojo) {
        return success(this.academyService.updateById(academyPojo));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestBody List<Long> ids) {
        this.academyService.remove(new QueryWrapper<AcademyPojo>().in("parent_id", ids));
        this.academyService.removeByIds(ids);
        allowfillterService.remove(new QueryWrapper<AllowfillterPojo>().in("categoryA_id", ids));
        fillterService.remove(new QueryWrapper<FillterPojo>().in("categoryA_id", ids));
        return success(true);
    }
}
