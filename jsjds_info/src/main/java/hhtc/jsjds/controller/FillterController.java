package hhtc.jsjds.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.FillterPojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.FillterService;
import hhtc.jsjds.vo.FilterConfigVO;
import hhtc.jsjds.vo.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 哪些专业不能报名非专业组(Fillter)表控制层
 *
 */
@RestController
@RequestMapping("fillter")
public class FillterController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private FillterService fillterService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @GetMapping
    public Result selectAll(Page<FilterConfigVO> page) {
        Page<FilterConfigVO> res = fillterService.getAllFilter(page);
        return new Result(true, StatusCodeEnum.OK, res);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.fillterService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param fillterPojo 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody FillterPojo fillterPojo) {
        return success(this.fillterService.save(fillterPojo));
    }

    /**
     * 修改数据
     *
     * @param fillterPojo 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody FillterPojo fillterPojo) {
        return success(this.fillterService.updateById(fillterPojo));
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestBody List<Long> ids) {
        return success(this.fillterService.removeByIds(ids));
    }
}
