package hhtc.jsjds.controller;


import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.FinalScorePojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.FinalscoreService;
import hhtc.jsjds.vo.FinalScoreVO;
import hhtc.jsjds.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;

/**
 * (Finalscore)表控制层
 *
 */
@Slf4j
@RestController
@RequestMapping("finalscore")
public class FinalscoreController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private FinalscoreService finalscoreService;

    /**
     * 分页查询所有数据
     *
     * @param page           分页对象
     * @param finalScorePojo 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<FinalScorePojo> page, FinalScorePojo finalScorePojo) {
        return success(this.finalscoreService.page(page, new QueryWrapper<>(finalScorePojo)));
    }

    @GetMapping("/all")
    public Result getAll(Page<FinalScoreVO> page) {
        Page<FinalScoreVO> result = finalscoreService.getAll(page);
        return new Result(true, StatusCodeEnum.OK, result);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.finalscoreService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param finalScorePojo 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody FinalScorePojo finalScorePojo) {
        return success(this.finalscoreService.save(finalScorePojo));
    }

    /**
     * 修改数据
     *
     * @param finalScorePojo 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody FinalScorePojo finalScorePojo) {
        return success(this.finalscoreService.updateById(finalScorePojo));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestParam("idList") List<Long> idList) {
        return success(this.finalscoreService.removeByIds(idList));
    }

    @GetMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response) {
        OutputStream os = null;// 取得输出流
        try {
            os = response.getOutputStream();
            response.reset();
            response.setCharacterEncoding("UTF-8");
            //响应内容格式
            response.setContentType("application/vnd.ms-excel");
            String fileName = "最终成绩" + System.currentTimeMillis() + ".xlsx";
            response.setHeader("content-Disposition", "attachment;fileName=" + fileName);// 设定输出文件头，这里fileName=后面跟的就是文件的名称，可以随意更改
            List<FinalScoreVO> list = finalscoreService.getFinalScore();
            EasyExcel.write(os, FinalScoreVO.class).sheet("最终成绩").doWrite(list);//sheet()里面的内容是工作簿的名称
        } catch (Exception e) {
            log.info("finalScore exportExcel异常：{}", e);
        }
    }
}
