package hhtc.jsjds.controller;


import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hhtc.jsjds.entity.FinalTeamPojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.listener.FinalTeamPojoListener;
import hhtc.jsjds.service.FinalTeamService;
import hhtc.jsjds.vo.FinalTeamVO;
import hhtc.jsjds.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;

/**
 * 最终参与答辩的队伍(FinalTeam)表控制层
 *
 */
@Slf4j
@RestController
@RequestMapping("finalTeam")
public class FinalTeamController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private FinalTeamService finalTeamService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @GetMapping
    public Result selectAll(Page<FinalTeamVO> page) {
        Page<FinalTeamVO> result = finalTeamService.getAll(page);
        return new Result(true, StatusCodeEnum.OK, result);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.finalTeamService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param finalTeamPojo 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody FinalTeamPojo finalTeamPojo) {
        return success(this.finalTeamService.save(finalTeamPojo));
    }

    /**
     * 修改数据
     *
     * @param finalTeamPojo 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody FinalTeamPojo finalTeamPojo) {
        return success(this.finalTeamService.updateById(finalTeamPojo));
    }

    /**
     * 删除数据
     *
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestBody List<Long> ids) {
        return success(this.finalTeamService.removeByIds(ids));
    }


    @PostMapping("/upload")
    public Result upload(@RequestParam("file") MultipartFile file) {
        try {
            EasyExcel.read(file.getInputStream(), FinalTeamPojo.class, new FinalTeamPojoListener(finalTeamService)).sheet().doRead();
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true, StatusCodeEnum.EXCEL_UPLOAD_FAIL);
        }
        return new Result(true, StatusCodeEnum.OK);
    }

    @GetMapping("/download")
    public void download(HttpServletResponse response) {
        OutputStream os = null;// 取得输出流
        try {
            os = response.getOutputStream();
            response.reset();
            response.setCharacterEncoding("UTF-8");
            //响应内容格式
            response.setContentType("application/vnd.ms-excel");
            String fileName = "答辩队伍" + System.currentTimeMillis() + ".xlsx";
            response.setHeader("content-Disposition", "attachment;fileName=" + fileName);// 设定输出文件头，这里fileName=后面跟的就是文件的名称，可以随意更改
            EasyExcel.write(os, FinalTeamPojo.class).sheet("队伍编号").doWrite(null);//sheet()里面的内容是工作簿的名称
        } catch (Exception e) {
            log.info("download异常：{}", e);
        }
    }
}
