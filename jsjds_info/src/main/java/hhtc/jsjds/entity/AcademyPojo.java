package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * (Academy)表实体类
 *
 * @author makejava
 * @since 2021-03-21 20:22:06
 */
@TableName("hhtc_academy")
public class AcademyPojo extends Model<AcademyPojo> {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String academyName;
    //标记个别专业比赛类别
    private String isSpecialty;

    private Integer parentId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAcademyName() {
        return academyName;
    }

    public void setAcademyName(String academyName) {
        this.academyName = academyName;
    }

    public String getIsSpecialty() {
        return isSpecialty;
    }

    public void setIsSpecialty(String isSpecialty) {
        this.isSpecialty = isSpecialty;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

}
