package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * 最终答辩顺序(FinalSeq)表实体类
 *
 * @author makejava
 * @since 2021-03-26 17:53:54
 */
@SuppressWarnings("serial")
@TableName("hhtc_final_seq")
public class FinalSeqPojo extends Model<FinalSeqPojo> {
    //队伍id
    private Integer teamId;
    //顺序

    private Integer sequence;
    //场地
    private Integer area;


    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.teamId;
    }
}
