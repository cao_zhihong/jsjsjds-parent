package hhtc.jsjds.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.io.Serializable;

/**
 * 最终参与答辩的队伍(FinalTeam)表实体类
 *
 * @author makejava
 * @since 2021-03-26 18:12:09
 */
@SuppressWarnings("serial")
@TableName("hhtc_final_team")
public class FinalTeamPojo extends Model<FinalTeamPojo> {

    @ExcelIgnore
    @TableId(type = IdType.AUTO)
    private Long id;

    @HeadStyle(horizontalAlignment = HorizontalAlignment.CENTER)//标题居中
    @ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)//内容居中
    @ColumnWidth(15)//单元格宽度
    @ExcelProperty(index = 0, value = "队伍编号")
    private Integer teamId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
