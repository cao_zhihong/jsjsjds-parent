package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * (Allowfillter)表实体类
 *
 * @author makejava
 * @since 2021-03-21 20:48:54
 */
@TableName("hhtc_allowfillter")
public class AllowfillterPojo extends Model<AllowfillterPojo> {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer academyId;

    private Integer categoryaId;


    public Integer getAcademyId() {
        return academyId;
    }

    public void setAcademyId(Integer academyId) {
        this.academyId = academyId;
    }

    public Integer getCategoryaId() {
        return categoryaId;
    }

    public void setCategoryaId(Integer categoryaId) {
        this.categoryaId = categoryaId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}
