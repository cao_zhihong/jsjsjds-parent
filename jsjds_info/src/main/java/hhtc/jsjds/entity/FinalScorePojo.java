package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.Date;

/**
 * (Finalscore)表实体类
 *
 * @author makejava
 * @since 2021-04-02 14:03:18
 */
@TableName("hhtc_finalscore")
public class FinalScorePojo extends Model<FinalScorePojo> {

    private Integer uid;

    private Integer worksId;
    //是否推荐，1为强烈推荐，2为推荐，3为不推荐
    private String recommend;

    private Integer score;

    private Date createTime;


    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getWorksId() {
        return worksId;
    }

    public void setWorksId(Integer worksId) {
        this.worksId = worksId;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


}
