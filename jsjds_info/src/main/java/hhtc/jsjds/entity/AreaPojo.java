package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * 答辩厂分配表(Area)表实体类
 *
 * @author makejava
 * @since 2021-03-25 21:11:21
 */
@TableName("hhtc_area")
public class AreaPojo extends Model<AreaPojo> {

    @TableId(type = IdType.AUTO)
    private Integer id;
    //场地名称
    private String areaName;
    //比赛分类id
    private Integer categoryId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }


}
