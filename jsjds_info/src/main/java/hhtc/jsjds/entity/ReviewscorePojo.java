package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * (Reviewscore)表实体类
 *
 * @author makejava
 * @since 2021-03-24 15:49:40
 */
@TableName("hhtc_reviewscore")
public class ReviewscorePojo extends Model<ReviewscorePojo> {

    private Integer worksId;

    private Integer uid;

    private Integer score;

    private String desc;


    public Integer getWorksId() {
        return worksId;
    }

    public void setWorksId(Integer worksId) {
        this.worksId = worksId;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
