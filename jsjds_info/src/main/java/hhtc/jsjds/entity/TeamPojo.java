package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * (Team)表实体类
 *
 * @author makejava
 * @since 2021-03-27 20:01:32
 */
@SuppressWarnings("serial")
@TableName("hhtc_name")
public class TeamPojo extends Model<TeamPojo> {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer uid;

    private String teacher;

    private Integer productionId;

    private Integer categoryaId;

    private Integer categorybId;

    private String invitationCode;

    private String createtime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Integer getProductionId() {
        return productionId;
    }

    public void setProductionId(Integer productionId) {
        this.productionId = productionId;
    }

    public Integer getCategoryaId() {
        return categoryaId;
    }

    public void setCategoryaId(Integer categoryaId) {
        this.categoryaId = categoryaId;
    }

    public Integer getCategorybId() {
        return categorybId;
    }

    public void setCategorybId(Integer categorybId) {
        this.categorybId = categorybId;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
