package hhtc.jsjds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * 哪些专业不能报名非专业组(Fillter)表实体类
 *
 * @author makejava
 * @since 2021-03-30 15:14:34
 */
@SuppressWarnings("serial")
@TableName("hhtc_fillter")
public class FillterPojo extends Model<FillterPojo> {

    @TableId(type = IdType.AUTO)
    private Integer id;
    //专业id
    private Integer academyId;
    //比赛大类id
    private Integer categoryaId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAcademyId() {
        return academyId;
    }

    public void setAcademyId(Integer academyId) {
        this.academyId = academyId;
    }

    public Integer getCategoryaId() {
        return categoryaId;
    }

    public void setCategoryaId(Integer categoryaId) {
        this.categoryaId = categoryaId;
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
