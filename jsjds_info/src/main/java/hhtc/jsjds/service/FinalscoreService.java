package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.FinalScorePojo;
import hhtc.jsjds.vo.FinalScoreVO;

import java.util.List;

/**
 * (Finalscore)表服务接口
 *
 * @author makejava
 * @since 2021-04-02 14:03:19
 */
public interface FinalscoreService extends IService<FinalScorePojo> {

    List<FinalScoreVO> getFinalScore();

    Page<FinalScoreVO> getAll(Page<FinalScoreVO> page);
}
