package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.FinalTeamPojo;
import hhtc.jsjds.vo.FinalTeamVO;

/**
 * 最终参与答辩的队伍(FinalTeam)表服务接口
 *
 * @author makejava
 * @since 2021-03-26 18:12:10
 */
public interface FinalTeamService extends IService<FinalTeamPojo> {

    Page<FinalTeamVO> getAll(Page<FinalTeamVO> page);

    void removeAll();

}
