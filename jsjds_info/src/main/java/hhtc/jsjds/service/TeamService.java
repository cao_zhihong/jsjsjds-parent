package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.TeamPojo;

/**
 * (Team)表服务接口
 *
 * @author makejava
 * @since 2021-03-27 20:01:33
 */
public interface TeamService extends IService<TeamPojo> {

}
