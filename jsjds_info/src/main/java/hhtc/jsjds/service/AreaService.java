package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.AreaPojo;
import hhtc.jsjds.vo.AreaGroupVO;
import hhtc.jsjds.vo.AreaVO;

import java.util.List;

/**
 * 答辩厂分配表(Area)表服务接口
 *
 * @author makejava
 * @since 2021-03-25 21:11:23
 */
public interface AreaService extends IService<AreaPojo> {

    Page<AreaVO> getAllArea(Page<AreaVO> page);

    List<AreaGroupVO> getGroupConcatIdAndCategoryId();
}
