package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.CategoryPojo;

import java.util.List;

/**
 * (Category)表服务接口
 *
 * @author makejava
 * @since 2021-03-22 19:41:56
 */
public interface CategoryService extends IService<CategoryPojo> {

    List<CategoryPojo> getAllRootCategory();

}
