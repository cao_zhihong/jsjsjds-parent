package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.FinalSeqPojo;
import hhtc.jsjds.vo.FinalSeqExcelVO;
import hhtc.jsjds.vo.FinalTeamSeqVO;

import java.util.List;
import java.util.Map;

/**
 * 最终答辩顺序(FinalSeq)表服务接口
 *
 * @author makejava
 * @since 2021-03-26 17:53:54
 */
public interface FinalSeqService extends IService<FinalSeqPojo> {

    void group();

    Page<FinalTeamSeqVO> getAll(Page<FinalTeamSeqVO> page, Map<String, Object> search);

    void removeAll();

    List<FinalSeqPojo> getFinalSeqGroupById(Integer id, Integer CategoryId);

    List<FinalSeqExcelVO> getExportExcelMsg(List<String> idList);
}
