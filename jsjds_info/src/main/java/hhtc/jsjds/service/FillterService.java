package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.FillterPojo;
import hhtc.jsjds.vo.FilterConfigVO;

/**
 * 哪些专业不能报名非专业组(Fillter)表服务接口
 *
 * @author makejava
 * @since 2021-03-30 15:14:34
 */
public interface FillterService extends IService<FillterPojo> {

    Page<FilterConfigVO> getAllFilter(Page<FilterConfigVO> page);
}
