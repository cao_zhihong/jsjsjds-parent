package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.common.P;
import hhtc.jsjds.entity.AllowfillterPojo;
import hhtc.jsjds.vo.FilterConfigVO;

/**
 * (Allowfillter)表服务接口
 *
 * @author makejava
 * @since 2021-03-21 20:48:54
 */
public interface AllowfillterService extends IService<AllowfillterPojo> {

    Page<FilterConfigVO> getAllowfillter(P page);
}
