package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.ReviewscorePojo;
import hhtc.jsjds.vo.FirstReviewScoreVO;

import java.util.List;

/**
 * (Reviewscore)表服务接口
 *
 * @author makejava
 * @since 2021-03-24 15:49:40
 */
public interface ReviewscoreService extends IService<ReviewscorePojo> {

    List<FirstReviewScoreVO> exportFirstReviewScore();

}
