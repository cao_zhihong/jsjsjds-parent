package hhtc.jsjds.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hhtc.jsjds.entity.AcademyPojo;

/**
 * (Academy)表服务接口
 *
 * @author makejava
 * @since 2021-03-21 20:22:06
 */
public interface AcademyService extends IService<AcademyPojo> {

}
