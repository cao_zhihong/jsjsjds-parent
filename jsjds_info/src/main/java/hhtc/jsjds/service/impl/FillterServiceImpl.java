package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.FillterDao;
import hhtc.jsjds.entity.FillterPojo;
import hhtc.jsjds.service.FillterService;
import hhtc.jsjds.vo.FilterConfigVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 哪些专业不能报名非专业组(Fillter)表服务实现类
 *
 * @author makejava
 * @since 2021-03-30 15:14:35
 */
@Service("fillterService")
public class FillterServiceImpl extends ServiceImpl<FillterDao, FillterPojo> implements FillterService {

    @Override
    public Page<FilterConfigVO> getAllFilter(Page<FilterConfigVO> page) {
        List<FilterConfigVO> list = this.baseMapper.getAllFilter(page);
        page.setRecords(list);
        return page;
    }
}
