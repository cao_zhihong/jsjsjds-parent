package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.AcademyDao;
import hhtc.jsjds.entity.AcademyPojo;
import hhtc.jsjds.service.AcademyService;
import org.springframework.stereotype.Service;

/**
 * (Academy)表服务实现类
 *
 * @author makejava
 * @since 2021-03-21 20:22:06
 */
@Service("academyService")
public class AcademyServiceImpl extends ServiceImpl<AcademyDao, AcademyPojo> implements AcademyService {

}
