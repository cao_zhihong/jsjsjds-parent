package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.CategoryDao;
import hhtc.jsjds.entity.CategoryPojo;
import hhtc.jsjds.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Category)表服务实现类
 *
 * @author makejava
 * @since 2021-03-22 19:41:56
 */
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryPojo> implements CategoryService {

    @Override
    public List<CategoryPojo> getAllRootCategory() {
        return this.baseMapper.selectList(new QueryWrapper<CategoryPojo>().eq("parent_id", 0));
    }
}
