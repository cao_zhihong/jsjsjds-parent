package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.FinalscoreDao;
import hhtc.jsjds.entity.FinalScorePojo;
import hhtc.jsjds.service.FinalscoreService;
import hhtc.jsjds.vo.FinalScoreVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Finalscore)表服务实现类
 *
 * @author makejava
 * @since 2021-04-02 14:03:19
 */
@Service("finalscoreService")
public class FinalscoreServiceImpl extends ServiceImpl<FinalscoreDao, FinalScorePojo> implements FinalscoreService {

    @Override
    public List<FinalScoreVO> getFinalScore() {
        return this.baseMapper.getFinalScore();
    }

    @Override
    public Page<FinalScoreVO> getAll(Page<FinalScoreVO> page) {
        List<FinalScoreVO> result = this.baseMapper.getAll(page);
        page.setRecords(result);
        return page;
    }
}
