package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.common.P;
import hhtc.jsjds.dao.AllowfillterDao;
import hhtc.jsjds.entity.AllowfillterPojo;
import hhtc.jsjds.service.AllowfillterService;
import hhtc.jsjds.vo.FilterConfigVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Allowfillter)表服务实现类
 *
 * @author makejava
 * @since 2021-03-21 20:48:54
 */
@Service("allowfillterService")
public class AllowfillterServiceImpl extends ServiceImpl<AllowfillterDao, AllowfillterPojo> implements AllowfillterService {

    @Autowired
    private AllowfillterDao allowfillterDao;

    @Override
    public Page<FilterConfigVO> getAllowfillter(P page) {
        Page<FilterConfigVO> p = new Page<>(page.getCurrPage(), page.getPageSize());
        List<FilterConfigVO> result = allowfillterDao.getAllowfillter(p);
        p.setRecords(result);
        return p;
    }
}
