package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.AreaDao;
import hhtc.jsjds.entity.AreaPojo;
import hhtc.jsjds.service.AreaService;
import hhtc.jsjds.vo.AreaGroupVO;
import hhtc.jsjds.vo.AreaVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 答辩厂分配表(Area)表服务实现类
 *
 * @author makejava
 * @since 2021-03-25 21:11:24
 */
@Service("areaService")
public class AreaServiceImpl extends ServiceImpl<AreaDao, AreaPojo> implements AreaService {

    @Override
    public Page<AreaVO> getAllArea(Page<AreaVO> page) {
        List<AreaVO> result = this.baseMapper.getAllArea(page);
        page.setRecords(result);
        return page;
    }

    @Override
    public List<AreaGroupVO> getGroupConcatIdAndCategoryId() {
        return this.baseMapper.getGroupConcatIdAndCategoryId();
    }
}
