package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.FinalTeamDao;
import hhtc.jsjds.entity.FinalTeamPojo;
import hhtc.jsjds.redis.RedisProvider;
import hhtc.jsjds.service.FinalTeamService;
import hhtc.jsjds.task.AsyncTask;
import hhtc.jsjds.vo.FinalTeamVO;
import hhtc.jsjds.vo.NotifyStudentSmsVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 最终参与答辩的队伍(FinalTeam)表服务实现类
 *
 * @author makejava
 * @since 2021-03-26 18:12:10
 */
@Slf4j
@Service("finalTeamService")
public class FinalTeamServiceImpl extends ServiceImpl<FinalTeamDao, FinalTeamPojo> implements FinalTeamService {

    @Autowired
    private RedisProvider redisProvider;

    @Override
    public Page<FinalTeamVO> getAll(Page<FinalTeamVO> page) {
        List<FinalTeamVO> result = this.baseMapper.getAll(page);
        page.setRecords(result);
        return page;
    }

    @Lazy
    @Autowired
    private AsyncTask asyncTask;

    @Override
    public void removeAll() {
        this.baseMapper.removeAll();
    }

}
