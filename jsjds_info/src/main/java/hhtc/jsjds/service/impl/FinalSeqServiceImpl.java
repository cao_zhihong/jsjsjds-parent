package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.FinalSeqDao;
import hhtc.jsjds.entity.FinalSeqPojo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.exception.XscoderException;
import hhtc.jsjds.service.FinalSeqService;
import hhtc.jsjds.task.AsyncTask;
import hhtc.jsjds.vo.FinalSeqExcelVO;
import hhtc.jsjds.vo.FinalTeamSeqVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 最终答辩顺序(FinalSeq)表服务实现类
 *
 * @author makejava
 * @since 2021-03-26 17:53:54
 */
@Service("finalSeqService")
public class FinalSeqServiceImpl extends ServiceImpl<FinalSeqDao, FinalSeqPojo> implements FinalSeqService {

    @Lazy
    @Autowired
    private AsyncTask asyncTask;

    /**
     * 分组规则：每个答辩场的答辩作品 会按大类进行答辩，大类答辩顺序随机
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void group() {
        try {
            //线程池异步调用
            asyncTask.shuffleGroup();
        } catch (Exception e) {
            e.printStackTrace();
            throw new XscoderException(StatusCodeEnum.GROUP_FAIL);
        }

    }

    @Override
    public Page<FinalTeamSeqVO> getAll(Page<FinalTeamSeqVO> page, Map<String, Object> search) {
        List<FinalTeamSeqVO> result = this.baseMapper.getAll(page, search);
        page.setRecords(result);
        return page;
    }

    @Override
    public void removeAll() {
        this.baseMapper.removeAll();
    }

    @Override
    public List<FinalSeqPojo> getFinalSeqGroupById(Integer id, Integer CategoryId) {
        return this.baseMapper.getFinalSeqGroupById(id, CategoryId);
    }

    @Override
    public List<FinalSeqExcelVO> getExportExcelMsg(List<String> idList) {
        return this.baseMapper.getExportExcelMsg(idList);
    }

}
