package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.TeamDao;
import hhtc.jsjds.entity.TeamPojo;
import hhtc.jsjds.service.TeamService;
import org.springframework.stereotype.Service;

/**
 * (Team)表服务实现类
 *
 * @author makejava
 * @since 2021-03-27 20:01:33
 */
@Service("teamService")
public class TeamServiceImpl extends ServiceImpl<TeamDao, TeamPojo> implements TeamService {

}
