package hhtc.jsjds.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hhtc.jsjds.dao.ReviewscoreDao;
import hhtc.jsjds.entity.ReviewscorePojo;
import hhtc.jsjds.service.ReviewscoreService;
import hhtc.jsjds.vo.FirstReviewScoreVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Reviewscore)表服务实现类
 *
 * @author makejava
 * @since 2021-03-24 15:49:40
 */
@Service("reviewscoreService")
public class ReviewscoreServiceImpl extends ServiceImpl<ReviewscoreDao, ReviewscorePojo> implements ReviewscoreService {

    @Override
    public List<FirstReviewScoreVO> exportFirstReviewScore() {

        return this.baseMapper.exportFirstReviewScore();
    }
}
