package hhtc.jsjds.securityhandler;

import com.alibaba.fastjson.JSON;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author
 *@Description:如果用户已经通过身份验证，试图访问受保护的(该用户没有权限的)资源
 */

public class GoAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    @CrossOrigin
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
//        RestResult restResult=new RestResult(false,"403",null,"您的权限不足，无法访问该资源");
        Map restResult = new HashMap();
        restResult.put("msg","DeniedHandler您的权限不足，无法访问该资源");
        httpServletResponse.getWriter().write(JSON.toJSONString(restResult));
        httpServletResponse.getWriter().flush();
    }
}
