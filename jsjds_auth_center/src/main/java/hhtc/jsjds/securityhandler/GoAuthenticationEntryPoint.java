package hhtc.jsjds.securityhandler;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author
 *@Description:它负责启动未经过身份验证的用户的身份验证过程(当他们试图访问受保护的资源
 */
public class GoAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    @CrossOrigin
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
//        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
//        httpServletResponse.getWriter().print("{\"code\":401,\"未登陆时无法访问该资源\":\""+e.getMessage()+"\"}");
//        httpServletResponse.getWriter().flush();
//        RestResult restResult=new RestResult(false,"403",null,"您的权限不足，无法访问该资源");
        httpServletResponse.setHeader("Content-Type", "application/json;charset=utf-8");
        Map restResult = new HashMap();
        restResult.put("msg","GoAuthenticationEntryPoint，鉴权中心无法访问该资源");
        httpServletResponse.getWriter().write(JSON.toJSONString(restResult));
        httpServletResponse.getWriter().flush();
    }
}
