package hhtc.jsjds.securityhandler;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author
 * @Description:如果身份验证失败时调用
 */
public class GoAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    @CrossOrigin
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setHeader("Content-Type", "application/json;charset=utf-8");
//        httpServletResponse.getWriter().print("{\"code\":1,\"message\":\""+e.getMessage()+"\"}");
//        httpServletResponse.getWriter().flush();
//        RestResult restResult=new RestResult(false,"401",null,"用户名或密码错误");
//        throw new XscoderException(StatusCodeEnum.WRONG_USERNAME_OR_PASSWORD);
        Map restResult = new HashMap();
        restResult.put("message","用户名或密码错误");
        restResult.put("flag",false);
        httpServletResponse.getWriter().write(JSON.toJSONString(restResult));
        httpServletResponse.getWriter().flush();
    }
}
