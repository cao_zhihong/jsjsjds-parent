package hhtc.jsjds.service.impl;

import hhtc.jsjds.entity.UserInfo;
import hhtc.jsjds.mapper.UserInfoMapper;
import hhtc.jsjds.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserinfoServiceImpl implements UserInfoService, UserDetailsService {

    @Autowired
    UserInfoMapper userInfoMapper;

    @Override
    public UserInfo loadUserByUsername(String s) throws UsernameNotFoundException {
        return userInfoMapper.loadUserByUsername(s);
    }
}
