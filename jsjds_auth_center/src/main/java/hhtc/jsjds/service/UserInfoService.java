package hhtc.jsjds.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserInfoService {
    UserDetails loadUserByUsername(String username);
}
