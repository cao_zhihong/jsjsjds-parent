package hhtc.jsjds.mapper;

import hhtc.jsjds.entity.UserInfo;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;


@Repository
public interface UserInfoMapper extends Mapper<UserInfo> {
    UserInfo loadUserByUsername(String username);
}
