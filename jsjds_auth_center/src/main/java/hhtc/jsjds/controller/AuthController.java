package hhtc.jsjds.controller;


import hhtc.jsjds.entity.UserInfo;
import hhtc.jsjds.enums.StatusCodeEnum;
import hhtc.jsjds.service.UserInfoService;
import hhtc.jsjds.utils.JwtUtils;
import hhtc.jsjds.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/auth")
public class AuthController {

//    @Autowired
//    RedisTemplate redisTemplate;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserInfoService userInfoService;



    // 装载BCrypt密码编码器
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }

    private static final String KEY_PREFIX= "registercode:phone";

    @PostMapping("/info")
    public Result getUserInfo(@RequestParam(required = true) String token) {
        String name = jwtUtils.getUsernameFromToken(token);
        UserInfo user = (UserInfo) userInfoService.loadUserByUsername(name);
        user.setPassword(null);
        return new Result(true, StatusCodeEnum.OK,user);
    }

    @PostMapping("/logout")
    public Result userLogout() {
        return new Result(true, StatusCodeEnum.OK);
    }

//    @RequestMapping("/registercode")
//    public Result getRegisterCode(@RequestParam(required = true) String phone) {
//        if(phone==null){
//            throw new XscoderException(StatusCodeEnum.BAD_REQUEST);
//        }
//        String code = CodeUtils.generateCode(6);
//        String key = KEY_PREFIX+phone;
//        if(redisTemplate.opsForValue().get(key)!=null){
//            throw new XscoderException(StatusCodeEnum.OPT_TOO_FAST);
//        }
//        redisTemplate.opsForValue().set(key,code,10, TimeUnit.MINUTES);
//        return new Result(true,StatusCodeEnum.SMSOK);
//    }
//
//    @PostMapping("/user")
//    public Result Register(UserInfo user, @RequestParam(required = true) String code) {
//        if(StringUtils.isBlank(code)){
//            throw new XscoderException(StatusCodeEnum.BAD_REQUEST);
//        }
//        String rediscode = redisTemplate.opsForValue().get(KEY_PREFIX+user.getPhone());
//        if((StringUtils.isBlank(rediscode))||!rediscode.equals(code)){
//            throw new XscoderException(StatusCodeEnum.WRONG_CODE);
//        }
//        user.setPassword(passwordEncoder().encode(user.getPassword()));
//        try{
//            userInfoService.registerNewUser(user);
//        }catch (Exception e){
//            throw new XscoderException(StatusCodeEnum.REGISTER_FAILED);
//        }
//        return new Result(true, StatusCodeEnum.REGISTER_SUCCESS);
//    }
//
//
//    @PostMapping("/wxlogin")
//    public Result wxLogin( @RequestParam(required = true) String wx_openid) {
//        String token = "";
//        Student student = userFeignClient.getStudentByWX_openid(wx_openid);
//        Map map = new HashMap();
//        map.put("openid",wx_openid);
//        if(student==null){
//            throw new XscoderException(StatusCodeEnum.STUDENT_NOT_BINDED);
//        }else if(student.getState().equals(StudentConfig.STUDENT_WAIT_BIND_PHONE+"")){
//            map.put("stu_id",student.getStu_id());
//            map.put("current",3);
//            return new Result(false,StatusCodeEnum.STUDENT_WAIT_BIND_PHONE,map);
//        }else{
//            List<SimpleGrantedAuthority> authorities = new ArrayList<>();
//            authorities.add(new SimpleGrantedAuthority("ROLE_STUDENT"));
//            try {
//                token = JwtUtils.generateStudentToken(student.getId(),student.getStu_id(),authorities,24*60*60);
//                map.put("token",token);
//            } catch (Exception e) {
//                e.printStackTrace();
//                throw new XscoderException(StatusCodeEnum.WX_LOGIN_FAILED);
//            }
//        }
//        return new Result(true,StatusCodeEnum.OK,map);
//    }


}
