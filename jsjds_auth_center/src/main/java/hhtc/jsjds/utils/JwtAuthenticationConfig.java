package hhtc.jsjds.utils;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@ToString
@Component
public class JwtAuthenticationConfig {

    @Value("${jsjds.security.jwt.url:/login}")
    private String url;

    @Value("${jsjds.security.jwt.header:Authorization}")
    private String header;

    @Value("${jsjds.security.jwt.prefix:Bearer}")
    private String prefix;

    @Value("${jsjds.security.jwt.expiration:#{2*24*60*60}}")
    private int expiration; // default 24 hours

    @Value("${jsjds.security.jwt.secret:otherpeopledontknowit}")
    private String secret;
}
