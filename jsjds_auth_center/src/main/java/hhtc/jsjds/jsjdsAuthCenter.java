package hhtc.jsjds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("hhtc.jsjds.mapper")
public class jsjdsAuthCenter {
    public static void main(String[] args) {
        SpringApplication.run(jsjdsAuthCenter.class, args);
    }
}
